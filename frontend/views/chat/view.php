<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;

use yii\widgets\Breadcrumbs;

$this->title = $model->Topic;;
$this->params['breadcrumbs'][] = ['label' => 'Chat Archives', 'url' => ['chat/archives']];
$this->params['breadcrumbs'][] = $this->title;


?>
<?php 
$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$popup = <<<JS
  $('.teal.button')
  .popup({
    on: 'click'
  })
;
JS;
$transcripts = <<<JS


jQuery("#transcriptsmodal").modal();

jQuery(".load_trans_btn").on("click", function(event) {
    event.preventDefault();
    jQuery("#transcriptsmodal").modal("show");
});
JS;

$this->registerJs($noconflict, \yii\web\View::POS_READY);
$this->registerJs($popup, \yii\web\View::POS_READY);
$this->registerJs($transcripts, \yii\web\View::POS_READY);

$this->registerJsFile('/js/tableHeadFixer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$jass = <<<JS
 $("#fixTable").tableHeadFixer();
JS;
$this->registerJs($jass, \yii\web\View::POS_READY);



?>
<?= $this->render('@frontend/views/templates/metro_register')?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="chat_trascript_page chat_details_page">
  <div class="row" id="podcasts_section">
    
     <!-- page-title -->

    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 foot" >
               <div class="row breath">
                    <?= Breadcrumbs::widget([
                         'homeLink' => [ 
                                  'label' => Yii::t('yii', 'Home'),
                                  'url' => Yii::$app->homeUrl,
                             ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    
                </div>
                  <div class="page-title">
    
               <span class="col-xs-12 col-sm-10"><h1 class="page-header"><?php echo $model->Topic; ?></h1></span>
      
          </div>
          <div class="load_trans">
                        <span class="load_trans_btn"> Load Chat Transcript </span>
                    </div>
               <?php if ($model->Podcast == NULL):?>
                 <div class="col-md-12  ">
                
                <div class="match_height  chat_view_sec">
                      
                    <div class="holder">
                                
                        <div class="row">

                            <div class="media main-chat-btns">
                        
                              <span class="make-responsive">
            <?php if($model->host->profile->avatar):?>
                <div class="host-avatar-holder">
               <?= Elements::image($model->host->profile->getImgSrc('avatar'), ['class' => 'img-responsive']) ?>
                  </div>
            <?php else:?>
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>
            </span>
                        </div><!-- row -->
                        
                        
                        
        <div class="chat_info list-group col-md-12 col-xs-12 col-lg-12 col-sm-12">
            <ul>
                <li class=" list-group-item active theme">
                    <span class="chat_info_left" >TOPIC: </span>
                    <span class="chat_info_rigth first"><?php echo ucfirst($model->Topic); ?></span>
                </li>
                <li class=" list-group-item topic">
                    <span class="chat_info_left" >Host: </span>
                    <span class="chat_info_rigth first"><?php  if($model->host->profile !== null){echo $model->host->profile->name ?> <?php echo $model->host->profile->lastname;}else{echo $model->hoststring;} ?></span>
                    <span class="chat_info_left" > 
                            <?php $ceomodal = Modal::begin([
                          'size' => Size::SMALL,
                          'header' => 'CEO\'s Profile',
                          'actions' => Elements::button('Close' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
                      ]); ?>
                    <?= $this->render('@frontend/views/user/profile/profilepopup', ['model'=>$model->host])?>

                    <?php $ceomodal::end(); ?>

                    <?= $ceomodal->renderToggleButton('View Host\'s Profile',['class' => '']) ?>
                    </span>
                </li>  
                <li class="list-group-item month">
                    <span class="chat_info_left">MONTH: </span>
                    <span class="chat_info_rigth"><?php echo Html::encode(Yii::$app->formatter->asDate($model->ChatDate,'MMMM')." ". date('Y', strtotime($model -> ChatDate))); ?></span>
                </li>
                <li class="list-group-item fulldetails">
                    <span class="chat_info_left">Description: </span>
                    <span class="chat_info_rigth"><?php echo $model->Description ?></span>
                </li>
                
            </ul>
         </div>

                    </div> <!-- holder -->

                </div><!-- chat_view_sec -->

             </div><!-- chat_info -->

              <?php else:?>
              <div class="col-md-6 match_height">
                <div class="load_trans">
                        <span class="podcast_trans_btn"> Podcast </span>
                    </div>
                <?php if ($model->Podcast): ?>
                <div class="row">
                   
                        <div class="embed-responsive embed-responsive-4by3 podcast">
                            <iframe src="//www.youtube.com/embed/<?php echo $model->Podcast?>" class="embed-responsive-item" allowfullscreen></iframe>
                        </div>
                </div>

                <?php endif; ?> 
            </div>

            <div class="col-md-6  ">
                
                <div class="match_height  chat_view_sec">
                      
                    <div class="holder">
                                
                        <div class="row">

                            <div class="media main-chat-btns">
                        
                              <span class="make-responsive">
            <?php if($model->host->profile->avatar):?>
                <div class="host-avatar-holder">
               <?= Elements::image($model->host->profile->getImgSrc('avatar'), ['class' => 'img-responsive']) ?>
                  </div>
            <?php else:?>
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>
            </span>
                             

                            </div><!-- media main-chat-btns -->

                        </div><!-- row -->
                        
                        
                        <div class="chat_info list-group col-md-12 col-xs-12 col-lg-12 col-sm-12">
            <ul>
                <li class=" list-group-item active theme">
                    <span class="chat_info_left" >TOPIC: </span>
                    <span class="chat_info_rigth first"><?php echo ucfirst($model->Topic); ?></span>
                </li>
                <li class=" list-group-item topic">
                    <span class="chat_info_left" >Host: </span>
                    <span class="chat_info_rigth first"><?php  if($model->host->profile !== null){echo $model->host->profile->name ?> <?php echo $model->host->profile->lastname;}else{echo $model->hoststring;} ?></span>
                    <span class="chat_info_left" > 
                            <?php $ceomodal = Modal::begin([
                          'size' => Size::SMALL,
                          'header' => 'CEO\'s Profile',
                          'actions' => Elements::button('Close' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
                      ]); ?>
                    <?= $this->render('@frontend/views/user/profile/profilepopup', ['model'=>$model->host])?>

                    <?php $ceomodal::end(); ?>

                    <?= $ceomodal->renderToggleButton('View Host\'s Profile',['class' => '']) ?>
                    </span>
                </li>  
                <li class="list-group-item month">
                    <span class="chat_info_left">MONTH: </span>
                    <span class="chat_info_rigth"><?php echo Html::encode(Yii::$app->formatter->asDate($model->ChatDate,'MMMM')." ". date('Y', strtotime($model -> ChatDate))); ?></span>
                </li>
                <li class="list-group-item fulldetails">
                    <span class="chat_info_left">Description: </span>
                    <span class="chat_info_rigth"><?php echo $model->Description ?></span>
                </li>
                
            </ul>
         </div>

                    </div> <!-- holder -->

                </div><!-- chat_view_sec -->

             </div><!-- chat_info -->
           <?php endif;?>
           
            </div>
            


            
        </div><!-- row -->
        
        <div id="transcriptsmodal" class="ui long modal">

            <div class="chat_transcript  panel panel-default scrollable" id="the_chat_transcript">
              
                <div class="panel-heading">CHAT TRANSCRIPT 
                
                   <?php echo Html::a('Download Transcript', ['/chat/chatpdf','id'=>$model->ID], [
                  'class'=>'btn btn-custom pull-right', 
                  'target'=>'_blank', 
              ]);?>
                
                </div> <!-- panel-heading -->

                <div class="table-responsive" id="table_parent">

                 <table class="table table-bordered table-striped table-hover" id="fixTable">
                 <thead>
                     <tr>
                         <th>Timestamp</th>
                         <th>Source</th>
                         <th>Message</th>
                     </tr>
                 </thead>
                 <tbody>
                     <?= $this->render('transcript', ['messages'=>$messages])?>
                </tbody>
                </table>

                </div><!-- table-responsive -->

            </div><!-- panel-default chat_transcript  -->

            <div class="actions"><div class="ui cancel right labeled icon button">Exit<i class="remove icon"></i></div></div>

            </div><!-- row -->

        </div><!--  container -->

    </div><!--  events_section -->
</div> <!-- row -->
