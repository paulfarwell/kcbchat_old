<?php
// use app\modules\demo\widgets\Example;
use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
$this->registerJs("var socket = io.connect('http://kcbchat.farwell-consultants.com:443/socket');", View::POS_END);
$this->registerJs("var chatRoom = ".json_encode($chatvariables).";", View::POS_HEAD, 'my-options');
$this->registerJsFile('/js/notification.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$popup = <<<JS
  $('.teal.button')
  .popup({
    on: 'click'
  })
;
JS;

$popover = <<<JS
  $('.user_settings')
  .popup({
    inline   : true,
    hoverable: true,
    position : 'bottom right',
  })
;
JS;

$jscrollto = <<<JS
 $(".icon-down").click(function() {
    $('html, body').animate({
        scrollTop: $("#area_active_chat").offset().top
    }, 1500);
});

JS;

/*
$js = <<<JS
$('#chat-form').submit(function() {

     var form = $(this);

     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               $("#message-field").val("");
          }
     });

     return false;
});
JS;
*/
$this->registerJs($noconflict, \yii\web\View::POS_READY);
$this->registerJs($popup, \yii\web\View::POS_READY);
$this->registerJs($popover, \yii\web\View::POS_READY);
$this->registerJs($jscrollto, \yii\web\View::POS_READY);
$this->registerJsFile('/js/farwell.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'Chatroom';
$this->params['breadcrumbs'][] = "";
?>
 <?php $reviewmodal = Modal::begin([
            'size' => Size::SMALL,
            'header' => 'Review and feedback',
        ]); ?>
          <?= $this->render('@frontend/views/chat/review_feedback', ['model'=>$model])?>

        <?php $reviewmodal::end(); ?>
<?= $this->render('@frontend/views/templates/chat_menu',['model'=>$model,'reviewmodal'=>$reviewmodal])?>
<div id="spec-chat-page" class="join_admin">
    <div class="row">
          <?= Breadcrumbs::widget([
               'homeLink' => [
                            'label' => Yii::t('yii', 'Home'),
                            'url' => Yii::$app->homeUrl,
                       ],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              ]) ?>
          </div>

  <div id="spec-chat-page" class="join_user">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 ">
         <noscript>
    <style type="text/css">
        .convo-body{display:none;}
    </style>
    <div class="row" style="color:#000000; text-align: center;">
    <h3 style="text-transform: initial;">Please activate javascript to use the platform. <a href="https://www.enable-javascript.com/" style="color:#064367" target="_blank">Click here for more</a></h3>
    </div>
</noscript>
      <div class="row convo-body">
      <br>
      <div class="col-md-2 pull-right ">
          <?= $this->render('/templates/notifications', ['module' => Yii::$app->getModule('user')]) ?>

      </div>
       <div class="conversation-area col-sm-12 col-md-12 col-lg-12 col-xs-12">

       <div class="chat-body" id="chat-body">

      <div id="area_active_chat">
          <?php  echo Html::Button('Click here to load old Messages ...',array('id'=>'loadOldMsgs', 'class'=>'btn btn-default')); ?>
      </div> <!-- area_active_chat -->



   </div> <!-- chat-body -->

    <div class="filtered_messages_container hidden-xs">
      <span class="filtered_messages_parent">

        <h3>Filtered Messages</h3>
      </span>

      <div id="area_filtered_chat" class="filtered_messages">
      </div> <!-- area_filtered_chat -->

    </div><!-- filtrd_msgs -->


      <span class="chat-form" id="area_chat_box">
       <div class="inasasput-group">
   <?= Html::beginForm(['/chat/join', 'id' => $model->ID], 'POST', [
                    'id' => 'chat-form',
                    'class' => 'input-group input-group-form-body col-lg-12 col-md-12 col-sm-12 col-xs-12',
                    'enctype' => 'multipart/form-data',
                     'style'=>'padding-left:10px'
                ]) ?>
                <span class="tag-text">
              <?=Html::textArea('span', '',[
                'id'=>'message-tag',
                'class' => 'chat-form-input-tag form-control col-xs-12 col-md-11 col-sm-12 col-lg-11','readonly'=>true, 'style'=>'display:none'])
              ?>
              <?= Html::HiddenInput('tag','',['id'=>'message-hidden']) ;?>
               <?= Html::HiddenInput('role','3',['id'=>'role-hidden']) ;?>
               <?= Html::HiddenInput('likes','',['id'=>'likes-hidden']) ;?>
            </span>
            <span class="tag-text">
               <select  id="mentions" style="display:none" class="form-control col-xs-12" multiple=""></select>
               <?= Html::HiddenInput('mention','',['id'=>'mention-hidden']) ;?>
            </span>
            <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
              
              <p class="lead emoji-picker-container">
              <span class="input-group ">
                <?= Html::textArea('message', null, [
                    'id' => 'message-field',
                    'class' => 'chat-form-input form-control ui-autocomplete-input ',
                    'placeholder' => 'Message...',
                    'style'=>'border:0px;resize:none',

                   
                ]) ?>
                

                <span class="input-group-btn">
                  <?= Html::submitButton(
                       Html::tag('span', '',['class' => 'fa fa-long-arrow-right']),
                       ['class' => 'chat-form-input-btn btn btn-default',
                        'id' => 'kcbsendbutton'
                  ]) ?>
                </span>

                 <span class="input-group-btn">
                <label for="emoji-input">
                <i data-emojiable="true" data-emoji-input="unicode" aria-hidden="true" style="color:#000" id="emojis">&nbsp</i>
              </label>
                
              </span>

                  <span class="input-group-btn">
                <label for="file-input">
                <i class="fa fa-paperclip fa-lg " aria-hidden="true" style="color:#000">&nbsp</i>
                <input type="file" class="file-input hide" name="file-input" id="file-input">
              </label>
                
              </span>
            
              </span>

              <div class="filename-container pull-right " id="filename-container"></div>
              </div>
              
                  
                <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-3 pull-right">
                      <?= Html::resetButton('Clear', [
                          'class' => 'btn btn-block btn-clear',
                          'id' => 'kcbclearbutton'
                      ]) ?>
                  </div>
                   

                <?= Html::endForm() ?>
         </div> <!-- input-group -->
      </span> <!-- chat-form -->



      </div> <!-- conversation-area -->
     </div>
     </div>
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <?= $this->render('/templates/host_notification', ['model' => $model->host]) ?>
       
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

           <?= $this->render('/templates/topic_notification', ['model' => $model]) ?>
        <?php $infomodal = Modal::begin([
            'size' => Size::SMALL,
            'header' => 'Topic Areas',
            'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
        ]); ?>
          <?= $this->render('topic_info_popup', ['model'=>$model])?>

        <?php $infomodal::end(); ?>
       
       </div>

       <div class="col-xs-12 col-md-10 col-sm-10 col-lg-10 col-lg-offset-1">
            <span class="make-responsive">
            <?php if(Yii::$app->user->identity->profile->avatar):?>
                <div class="avatar_holder">

                <?= Html::img('@web/images/avatar/'.Yii::$app->user->identity->profile->avatar, ['class' => 'img-responsive']); ?>
                  </div>
            <?php else:?>
              <div class="avatar_holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>


            </span>
            <div class="side-link">
            <i class="fa fa-plus" style="color:#064367;margin-bottom:7px"></i><?= Html::a(' Edit my personal profile', ['/user/settings/myprofile'], ['class'=>'side-link']) ?>
          </div>
         <!--  <form>
          <input type="file" id="file" name="file" class="btn btn-upload">
        </form> -->
          </div>
          <div class="online-user-container col-xs-12 col-md-12 col-lg-12 c0l-sm-12">

        <div class="online-users-intro">

              <span id="user_stats"> </span><!-- <span id="toggle_userslist" class="orange-header btn btn-default btn-sm">Online/ Active Users  </span> -->
              <!-- <button class="orange-header btn btn-default btn-sm">Active Users</button> -->
        </div>
        <hr>
        <div class="online-users-list-container">
            <div class="avatar-panel">
                  <!-- <div id="user_count">

                  </div> --><!-- user_count -->
                  <table class="online-user-panel table table-responsive table-condensed" id="users_online">
                      <tbody class="test" id="user_feed">

                      </tbody>
                  </table>

                  <table class="online-user-panel table table-responsive table-condensed" id="users_active">
                      <tbody class="test" id="user_feed_active">

                      </tbody>
                  </table>

            </div><!-- avatar-panel -->
        </div><!-- online-users-list-container -->

        

      </div>
     </div>
   </div>
</div><!-- #spec-chat-page -->
