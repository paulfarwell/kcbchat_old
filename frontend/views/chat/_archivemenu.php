<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\helpers\Size;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



/* equal heights */
$this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS
 $('.archive_chat_holder').matchHeight();
JS;
$this->registerJs($js, \yii\web\View::POS_READY);

$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$this->registerJs($noconflict, \yii\web\View::POS_READY);
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="col-xs-11 col-sm-6 col-md-6 col-lg-6">
      <span class="search_btn hidden-md hidden-lg hidden-sm" >

        <?php $searchmodal = Modal::begin([
            'size' => Size::LARGE,
            'header' => 'Filter Archives',
            'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
        ]); ?>
          <?= $this->render('_archivesearch', ['model' => $searchModel]);?>

        <?php $searchmodal::end(); ?>
        <?= $searchmodal->renderToggleButton(Elements::icon('search')) ?>

        </span>
        
        <div class=" hidden-xs">
                <?php echo $this->render('_archivesearch', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    
    
    <div class="collapse navbar-collapse" id="myNavbar">
      
      <ul class="nav navbar-nav navbar-right ">
      
        <li><a href="<?= Url::toRoute(['/',])?>" ><span class="fa fa-home"></span>Home</a></li>
  <li><a href="<?= Url::toRoute(['chat/enter'])?>"><span class="fa fa-comment-o"></span>Enter Chat Room</a> </li>
    <?php if (Yii::$app->user->isGuest):?>
    <li ><a href="<?= Url::toRoute(['site/login'])?>" ><span class="fa fa-lock "></span>Login</a></li>
  <?php else:?>
    <li ><a href="<?= Url::to(['/chat/archives'])?>" ><span class="fa fa-archive " data-method="post"></span>Chat Archive</a></li>
   <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span>MY PROFILE<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?= Url::to(['/user/settings/myprofile'])?>"><span class="fa fa-user"></span>My Profile</a></li>
                  <li ><a href="<?= Url::to(['/site/logout'])?>" data-method="post" ><span class="fa fa-lock " ></span>Logout</a></li>
                </ul>
              </li>
  <?php endif;?>
   <li><a href="<?= Url::to(['/site/help'])?>"><span class="fa fa-question"></span>Help</a></li>
    
      </ul>
    </div>
  </div>
</nav>