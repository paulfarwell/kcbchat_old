<?php
namespace frontend\models;

use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use dektrium\user\models\User as BaseUser;

class RegistrationForm extends BaseRegistrationForm
{
    /**
     * Add a new field
     * @var string
     */
    public $name;
    public $lastname;
    public $country;
    public $city;
    public $confirm_password;
    public $ageagree;
    public $termsagree;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['usernameRequired']);
        $rules[] = [['name','lastname','country','city','confirm_password'], 'required'];
        $rules[] = [['name','city'], 'string', 'max' => 255];
		$rules[] = ['lastname', 'string', 'max' => 255];
        $rules[] = [['country','ageagree','termsagree'], 'integer'];
        $rules[] = ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ];
        $rules[] = ['termsagree', 'required', 'requiredValue' => 1, 'message'=>"You have to agree with the terms and conditions" ];
        $rules[] = ['ageagree', 'required', 'requiredValue' => 1, 'message'=>"You have to be 18 years and above" ];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = \Yii::t('user', 'First Name');
        $labels['lastname'] = \Yii::t('user', 'Last Name');
        $labels['country'] = \Yii::t('user', 'Country');
        $labels['city'] = \Yii::t('user', 'City');
        $labels['ageagree'] = \Yii::t('user', 'I am 18 years and above');
        $labels['termsagree'] = \Yii::t('user', 'I agree with the terms and conditions');
        $labels['confirm_password'] = "Confirm Password";
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(BaseUser $user)
    {
        // here is the magic happens
        $user->setAttributes([
            'email'    => $this->email,
            'username' => $this->email,
            'password' => $this->password,
        ]);
        /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'name' => $this->name,
            'lastname' => $this->lastname,
            'country' => $this->country,
            'city' => $this->city,
            'ageagree' => $this->ageagree,
            'termsagree' => $this->termsagree,
            'confirm_password' => $this->confirm_password,
        ]);
        $user->setProfile($profile);
    }
}