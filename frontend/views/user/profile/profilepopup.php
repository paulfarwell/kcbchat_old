<?php
/**
 * var $model  user model
 * var $profile the user profile model
 */
 //print_r($model);
use Zelenin\yii\SemanticUI\Elements;
use yii\helpers\Html;
?>

<?php if($model->profile == null):?>
<div class="profpopupholder">
	<div class="top">
		<span class="make-responsive">
              <div class="image_holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            </span>
		
		<div class="details">
			<table class="detailstable">
				<tr>
					<td>Name:</td><td class="ceo-name"><?php echo $model->first_name. ' '. $model->last_name;?></td>
				</tr>
				<tr>
					<td>Organization:</td><td>KCB</td>
				</tr>
				<tr>
					
				</tr>
												
			</table>
			
		</div> <!--end details-->
		<div class="clearfix"></div>
	</div><!--end top-->
	<div class="bio">
		<label style="font-size:1.3em;padding-left: 4px;">Biography:</label>
	</div><!--end end bio-->
</div>

<?php else:?>	
<div class="profpopupholder">
	<div class="top">
		<span class="make-responsive">

		
            <?php if($model->profile->avatar):?>
                <div class="host-avatar-holder">
                	<?= Html::img('@web/images/avatar/'.$model->profile->avatar, ['class' => 'img-circle img-responsive']) ?> 
                	
                  </div>
            <?php else:?>
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-circle img-responsive'])?>
                </div>
            <?php endif;?>
            </span>
		<!-- <div class="pic">
			<?= Elements::image($model->profile->getImgSrc('avatar'), ['width' => '200px']) ?>
		</div> --><!--end pic-->
		<div class="details">
			<table class="detailstable">
				<tr>
					<td>Name:</td> <td class="ceo-name"><?= $model->profile->name ?> <?= $model->profile->lastname ?></td>
				</tr>
				<tr>
					<td>Organization:</td> <td><?php echo $model->profile->organization ?></td>
				</tr>
				<tr>
					<td>Title:</td><td><?php echo $model->profile->title ?></td>
				</tr>
				<!--
				<tr>
					<td>
						<?php # if($profile->twitter): ?>
						<a href="https://twitter.com/<?php # echo $profile -> twitter; ?>" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow CEo</a>
						<?php # endif; ?>
					</td>
				</tr>	-->											
			</table>
			
		</div> <!--end details-->
		<div class="clearfix"></div>
	</div><!--end top-->
	<div class="bio">
		<label style="font-size:1.3em; padding-left: 4px;">Biography:</label> <?php echo $model->profile->bio ?>
	</div><!--end end bio-->
</div>

<?php endif;?>