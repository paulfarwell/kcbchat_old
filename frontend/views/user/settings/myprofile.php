<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use dektrium\user\widgets\Connect;
use Zelenin\yii\SemanticUI\Elements;
/*
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */
// echo '<pre>',print_r($model),'</pre>';
// die();

$this->title = Yii::t('user', ' Profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@frontend/views/templates/profile_menu')?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="row user_profile">
<span class="page-title">
  <!-- <span class="col-xs-12 col-sm-10"><h1 class="page-header"><?= Html::encode($this->title)?></h1></span> -->

  <!-- <span class="loginout col-xs-12 col-sm-2">
    <?php if (Yii::$app->user->isGuest):?>
      <a class="btn-login" href="<?= Url::to(['site/register', '#' => 'login'])?>"> Log In </a>
      <?php else:?>
      <a class="btn-login" href="<?= Url::to(['user/security/logout'])?>"  data-method="post"> Log Out  </a>
    <?php endif; ?>
    
  </span> --><!--  loginout -->
</span> <!-- page-title -->

    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [ 
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9">

            <div class="profile_view">
               

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-md-offset-4 col-lg-offset-4">
                        <span class="make-responsive">
                        <?php if($model->avatar):?>
                            <div class="image_holder">

                            <?= Html::img('@web/images/avatar/'.$model->avatar, ['class' => 'img-responsive']); ?>
                              </div>
                        <?php else:?>
                          <div class="image_holder">
                               <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                            </div>
                        <?php endif;?>
                        </span>
                    </div>

                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-md-offset-2 col-lg-offset-2 ">
                          <hr class="profile-view-hr">
                      <ul class="profile-view-ul">
                      <li>
                        <?php if ($model->name): ?>
                           <label ><?= $model->getAttributeLabel('First Name:') ?></label> 
                            <span ><?= ucfirst($model->name) ?></span>
                        <?php else:?>
                            <label > <?= $model->getAttributeLabel('name:') ?></label>
                       
                        <?php endif; ?>

                      </li>
                      <li>
                        <?php if ($model->lastname): ?>
                              <label ><?= $model->getAttributeLabel('Last Name:') ?> </label> 
                              <span ><?= ucfirst($model->lastname) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('lastname:') ?></label>
                       
                        <?php endif; ?>
                      </li>
                      <li>
                        <?php if ($model->public_email): ?>
                        <label ><?= $model->getAttributeLabel('Email:') ?> </label> 
                              <span ><?= ucfirst($model->public_email) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Email:') ?></label>
                       
                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if ($model->employee_id): ?>
                        <label ><?= $model->getAttributeLabel('Staff Number:') ?> </label> 
                              <span ><?= ucfirst($model->employee_id) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Staff Number:') ?></label>
                       
                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if ($model->title): ?>
                        <label ><?= $model->getAttributeLabel('Title:') ?> </label> 
                              <span ><?= ucfirst($model->title) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('title:') ?></label>
                       
                        <?php endif; ?>
                        </li>
                      <li>
                         <?php if ($model->organization): ?>
                       <label ><?= $model->getAttributeLabel('Organization:') ?> </label> 
                              <span ><?= ucfirst($model->organization) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('organization:') ?></label>
                       
                        <?php endif; ?>
                      </li>
                      <li>
                        <?php if ($model->branch): ?>
                        <label ><?= $model->getAttributeLabel('Branch:') ?> </label> 
                              <span ><?= ucfirst($model->branch) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Branch:') ?></label>
                       
                        <?php endif; ?>
                        </li>
                        
                        <li>
                        <?php if($model->country):?>
                        <label ><?= $model->getAttributeLabel('Country:') ?> </label> 
                              <span ><?= ucfirst($model->country) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Country:') ?></label>
                       
                        <?php endif; ?>
                        </li>
                        <li>
                        <?php if ($model->city): ?>
                        <label ><?= $model->getAttributeLabel('County/Province:') ?> </label> 
                              <span ><?= ucfirst($model->city) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('city:') ?></label>
                       
                        <?php endif; ?>
                         </li>
                         <li>
                        <?php if ($model->bio): ?>
                        <label ><?= $model->getAttributeLabel('Bio:') ?> </label> 
                              <span ><?= ucfirst($model->bio) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('bio:') ?></label>
                       
                        <?php endif; ?>
                         </li>
                          <li>
                        <?php if ($model->hobbies): ?>
                        <label ><?= $model->getAttributeLabel('Hobbies:') ?> </label> 
                              <span ><?= ucfirst($model->hobbies) ?></span>
                        <?php else:?>
                            <label ><?= $model->getAttributeLabel('Hobbies:') ?></label>
                       
                        <?php endif; ?>
                         </li>
                         </ul>
                    <!-- form-horizontal form-white -->

                </div>

            </div>

        </div>

   </div><!--  container --> 
</div><!-- user_profile -->
