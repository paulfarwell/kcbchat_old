<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Edition;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Edition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="edition-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'EditionName')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'Theme')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'Year')->widget(
	    DatePicker::className(), [
	        // inline too, not bad
	         'inline' => false, 
	         // modify template for custom rendering
	        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
	        'clientOptions' => [
	        	'autoclose' => true, 
	            'format' => 'yyyy',
	            'viewMode' => 'years',
	            'minViewMode' => 'years'
	        ]
	]);?>

    <?= $form->field($model, 'Quarter')->dropDownList(Edition::quarterAlias(),['prompt'=>'Select...']) ?>

	<?= $form->field($model, 'Current')->dropDownList(Edition::currentAlias(),['prompt'=>'Select...'])  ?>	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
