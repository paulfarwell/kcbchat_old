<?php

namespace backend\controllers;

use Yii;
use common\models\Chat;
use backend\models\ChatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class ChatController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate()
    {
        $model = new Chat();
        $uploadPath = \Yii::getAlias('@frontend/web/chatlogs/');
        $bannerPath = \Yii::getAlias('@frontend/web/chatBanners/');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $log = UploadedFile::getInstance($model, 'Log');
            $file = UploadedFile::getInstance($model, 'chat_banner');
            if (!empty($log)) {
                $logname =  uniqid() . preg_replace('/\s+/', '_', $log->name);
                if ($log->saveAs($uploadPath . '/' . $logname)) {
                    $model->Log= $logname;
                }
            }
            if (!empty($file)) {
                $bannername =  uniqid() . preg_replace('/\s+/', '_', $file->name);
                if ($file->saveAs($bannerPath . '/' . $bannername)) {
                    $model->chat_banner= $bannername;
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Chat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $uploadPath = \Yii::getAlias('@frontend/web/chatlogs/');
         $bannerPath = \Yii::getAlias('@frontend/web/chatBanners/');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $log = UploadedFile::getInstance($model, 'Log');
             $file = UploadedFile::getInstance($model, 'chat_banner');
            if (!empty($log)) {
                $logname =  uniqid() . preg_replace('/\s+/', '_', $log->name);
                if ($log->saveAs($uploadPath . '/' . $logname)) {
                    $model->Log= $logname;
                }
            }
             if (!empty($file)) {
                $bannername =  uniqid() . preg_replace('/\s+/', '_', $file->name);
                if ($file->saveAs($bannerPath . '/' . $bannername)) {
                    $model->chat_banner= $bannername;
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            }
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Chat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
