<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;

/**
 * @var yii\web\View              $this
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

$this->title = Yii::t('user', 'Help');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/farwell.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$settings = Yii::$app->settings;
$settings->clearCache();
$reginfo = $settings->get('Theme.registrationDescription');
?>

<!-- Header -->
<?= $this->render('@frontend/views/templates/help_menu') ?>

<section class="chat-details-container ">
    <div class="container section-register">

        <div class="row">
 
            <div class="col-md-8 col-xs-12 col-sm-12 col-lg-8 col-md-offset-2 col-lg-offset-2" id="register">

    <div class="accordion" id="accordion2">
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
Click on the Login button to enter chatroom
</a>

</div>
<div id="collapseOne" class="accordion-body collapse">
<div class="accordion-inner">
<?= Html::img('@web/images/help/home.png',['class' => 'img-responsive'])?>
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
Login using your KCB Bank Group email address
</a>
</div>
<div id="collapseTwo" class="accordion-body collapse">
<div class="accordion-inner">
<?= Html::img('@web/images/help/register.png',['class' => 'img-responsive'])?>
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">
Enter Chatroom
</a>
</div>
<div id="collapseThree" class="accordion-body collapse">
<div class="accordion-inner">
	<?= Html::img('@web/images/help/chatroom.png',['class' => 'img-responsive'])?>
<p>Once you bhave successfully Login, click on the 'Enter Chatroom buttton to join a chat</p>
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">
Create/Edit your profile
</a>
</div>
<div id="collapseFour" class="accordion-body collapse">
<div class="accordion-inner">
<?= Html::img('@web/images/help/edit.png',['class' => 'img-responsive'])?>
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion5" href="#collapseFive">
How to chat on the platform
</a>
</div>
<div id="collapseFive" class="accordion-body collapse">
<div class="accordion-inner">
	<?= Html::img('@web/images/help/chat.png',['class' => 'img-responsive'])?>
Type your message in the box above and click the 'send or hit Enter button on your keyboard'
</div>
</div>
</div>
<div class="accordion-group">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#collapseSix">
Access Chat Archives
</a>
</div>
<div id="collapseSix" class="accordion-body collapse">
<div class="accordion-inner">
<?= Html::img('@web/images/help/archives.png',['class' => 'img-responsive'])?>
</div>
</div>
</div>
</div>




            </div><!-- col-md-6 -->

         </div><!-- row -->


 </div> <!-- main container -->
</section> <!-- container-section -->



