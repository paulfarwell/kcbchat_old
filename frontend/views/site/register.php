<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;

/**
 * @var yii\web\View              $this
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

$this->title = Yii::t('user', 'Register & Login');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/farwell.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$settings = Yii::$app->settings;
$settings->clearCache();
$reginfo = $settings->get('Theme.registrationDescription');
?>

<!-- Header -->
<?= $this->render('@frontend/views/templates/metro_register') ?>

<section class="chat-details-container container-grey">
    <div class="container section-register">

        <div class="row">
 
            <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6 col-md-offset-3 col-lg-offset-3" id="register">


            
                    <div class="registration-form form-white">

                        <h1 >Registration </h1>
                       
                                <?php $form = ActiveForm::begin([
                                    'id' => 'registration-form',
                                    'class' => 'form-horizontal',
                                    'layout' => 'horizontal',
                                    'fieldConfig' => [
                                        'horizontalCssClasses' => [
                                            'label' => 'col-xs-12 col-sm-3',
                                            'offset' => '',
                                            'wrapper' => 'col-xs-12 col-sm-12 col-md-12 col-lg-10',
                                            'error' => '',
                                            'hint' => '',
                                        ],
                                        'horizontalCheckboxTemplate'=>"<div class=\"form-group\">\n<div class=\"checkbox\">\n<label class=\"col-sm-10 col-xs-8 col-md-12 col-lg-10 control-label\">\n{labelTitle}\n{error}\n</label>\n<div class=\"col-sm-2 col-xs-3\">\n{input}\n</div>\n</div>\n</div>\n{hint}",
                                        
                                    ],
                                ]); ?>

                                <?= $form->field($model, 'name')->input('name',['placeholder'=>'First Name'])->label(false) ?>

                                <?= $form->field($model, 'lastname')->input('lastname',['placeholder'=>'Last Name'])->label(false) ?>

                                <?= $form->field($model, 'email')->input('email',['placeholder'=>'Email'])->label(false) ?>

                               <!-- <?= $form->field($model, 'username') ?>-->

                                <?= $form->field($model, 'password')->passwordInput()->input('password',['placeholder'=>'Password'])->label(false) ?>

                                <?= $form->field($model, 'confirm_password')->passwordInput()->input('password',['placeholder'=>'Confirm Password'])->label(false) ?>

                                <?= $form->field($model, 'country')
                                        ->dropDownList(
                                            ArrayHelper::map(Countries::find()->orderBy('CountryName')->all(), 'ID', 'CountryName'),         
                                            ['prompt'=>'Select Country']   
                                        )->label(false);
                                 ?>

                                <?= $form->field($model, 'city')->input('city',['placeholder'=>'City'])->label(false) ?>
                                
                                 <?= $form->field($model, 'ageagree')->checkbox() ?>

                                 <?= $form->field($model, 'termsagree')->checkbox(); ?>
                                

                                <p class="email_note">* The system will send an email. Please click to authenticate your Chat registration</p>

                                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-custom sign-up']) ?>

                                <?php ActiveForm::end(); ?>

                    </div><!-- registration-form -->

            </div><!-- col-md-6 -->

         </div><!-- row -->


 </div> <!-- main container -->
</section> <!-- container-section -->

<?= $this->render('login', ['model'=>$loginmodel,'module'=>$module])?>

