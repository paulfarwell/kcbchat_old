<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\widgets\Menu;

/** @var dektrium\user\models\User $user */
$user = Yii::$app->user->identity;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;

?>

<div class="panel panel-default profile_menu">
    <div class="panel-body" style="background-color:#ffffff;">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => Yii::t('user', 'Enter Chatroom'), 'url' => ['/chat/enter']],
                ['label' => Yii::t('user', 'My Profile'), 'url' => ['/user/settings/myprofile']],
                ['label' => Yii::t('user', 'Edit My Profile'), 'url' => ['/user/settings/profile']],
                // ['label' => Yii::t('user', 'Change My Password'), 'url' => ['/user/settings/account']],
                // ['label' => Yii::t('user', 'Social Networks'), 'url' => ['/user/settings/networks'], 'visible' => $networksVisible],
            ],
        ]) ?>
    </div>
</div>
