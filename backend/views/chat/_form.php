<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Edition;
use common\models\Chat;
use dosamigos\datepicker\DatePicker;
use dosamigos\ckeditor\CKEditor;
use kartik\time\TimePicker;
use frontend\models\User;
use backend\models\Host;


/* @var $this yii\web\View */
/* @var $model common\models\Chat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'Edition')->dropDownList(ArrayHelper::map(Edition::find()->all(),'ID','EditionName'),['prompt'=>'Select Edition']) ?>
    <?= $form->field($model, 'ChatDate')->widget(
        DatePicker::className(), [
            // inline too, not bad
             'inline' => false, 
             // modify template for custom rendering
            //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true, 
                'format' => 'yyyy-mm-dd'
            ]
    ]);?>

    <?= $form->field($model, 'StartTime')->widget(

        TimePicker::classname(), [
            'options' => [
                'readonly' => true,
            ],
            'pluginOptions' => [
                'showMeridian' => false,
            ]
    ]); 

    ?>

    <?= $form->field($model, 'EndTime')->widget(

        TimePicker::classname(), [
            'options' => [
                'readonly' => true,
            ],
            'pluginOptions' => [
                'showMeridian' => false,
            ]
    ]); 

    ?>


    <?= $form->field($model, 'Topic')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'Host')->dropDownList(ArrayHelper::map(Host::find()->all(),'id','CeoString'),['prompt'=>'Select Chair']) ?> -->

     <?= $form->field($model, 'chatHost')->dropDownList(ArrayHelper::map(Host::find()->all(),'id','CeoString'),['prompt'=>'Select Chair']) ?> 


    <?= $form->field($model, 'Description')->widget(CKEditor::className(), [ 
            'options' => ['rows' => 6],
            'preset' => 'custom',
            'clientOptions' => [
                'toolbar' => [
                    [
                        'name' => 'row1',
                        'items' => [
                            'Source', '-',
                            'Bold', 'Italic', 'Underline', 'Strike', '-',
                            'Subscript', 'Superscript', 'RemoveFormat', '-',
                            'TextColor', 'BGColor', '-',
                            'NumberedList', 'BulletedList', '-',
                            'Outdent', 'Indent', '-', 'Blockquote', '-',
                            'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'list', 'indent', 'blocks', 'align', 'bidi', '-',
                            'Link', 'Unlink', 'Anchor', '-',
                            'ShowBlocks', 'Maximize',
                        ],
                    ],
                    [
                        'name' => 'row2',
                        'items' => [
                            'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'Iframe', '-',
                            'NewPage', 'Print', 'Templates', '-',
                            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-',
                            'Undo', 'Redo', '-',
                            'Find', 'SelectAll', 'Format', 'Font', 'FontSize',
                        ],
                    ],
                ],
            ],
            ])?>

    <?= $form->field($model, 'Status')->dropDownList(Chat::chatStatus(),['prompt'=>'Select Status']) ?>

    <?= $form->field($model, 'Log')->fileInput() ?>

    <?= $form->field($model, 'Podcast')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ParticipantsLimt')->textInput() ?>
    <?= $form->field($model, 'chat_banner')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
