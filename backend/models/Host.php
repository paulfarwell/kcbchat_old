<?php
namespace backend\models;
 
use Yii;
use common\models\Chat;
 
class Host extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hosts';
    }
     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['email','first_name', 'last_name'], 'required'],
            ['email', 'email'],
            [['first_name', 'last_name'], 'string', 'max' => 255]
            
          
        ];
    }

    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['ID' => 'Chat']);
    }
 

      public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['public_email' => 'email']);
    }

       public function getCeoString()
      {
        
      return $this->first_name . " " . $this->last_name ;
           
      }

       public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
}