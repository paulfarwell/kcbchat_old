<?php
$this->registerJsFile('/js/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/topic-notification-helper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
use yii\helpers\Html;

?>
<div class="topic-notifications-wrapper">
	
	<ul id="nav" class="topic-notifications_ul">
		<li id="topic-notification_li" class="pull-right">
		<button class="ui info_launcher host-button  button"  id="topic-notificationLink">
		    <?php echo Html::a(Html::img('@web/images/icons/Topic2.png', ['class' => ' img-responsive','style'=>'width:75%'])); ?>
		</button>
		
	<!--
		<a href="#" id="notificationLink"><span id="notification_count">3</span><i class="nav-icon fa fa-bullhorn"></i> 
		<span class="small-screen-text">Notifications</span></a>-->

		<div id="topic-notificationContainer">
			<div class="topic-notificationContainer">
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
					<?= Html::img('@web/images/icons/close.png',['class' => 'img-responsive', 'id' =>'my_topic_close'])?>
				</div>
				 <h2>Topic Areas</h2> 

				    <h3 style="display:inline-flex;">
				    	<?= Html::img('@web/images/icons/topic-chat.png',['class' => 'img-responsive','style'=>'width:40px; height:40px;'])?>
				   	<span style="padding-left:10px"><?php echo $model->Topic ?></span></h3>
				 <ul>
				 	<li><b>Date:</b> <span><?php echo Html::encode($model->ChatDate); ?></span></li>
				 	<li><b>Time:</b> <span><?php echo Html::encode(Yii::$app->formatter->asDatetime($model->StartTime,'h:mm a')." - ".Yii::$app->formatter->asDatetime($model->EndTime,'h:mm a')); ?></span></li>
				 	<li><b>Host:</b> <?php echo Html::encode($model->host->Ceostring); ?></li>
				 	<li><b>Description:</b> <span><p style="text-align:none"><?php echo $model->Description ?></p></span></li>
				 </ul>
			</div>
		<!--<div id="notificationTitle">New Messages</div>-->
		

		</li>
	</ul>
</div>