<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chat Host\'s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add a Host', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

      <table border="1" class="table table-striped table-bordered">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Actions</th>
    </tr>
    <?php foreach($model as $field){ ?>
    <tr>
        
        <td><?= $field->first_name; ?></td>
        <td><?= $field->last_name; ?></td>
        <td><?= $field->email; ?></td>
        <td><?= Html::a("Edit", ['host/edit', 'id' => $field->id]); ?> | <?= Html::a("Delete", ['host/delete', 'id' => $field->id]); ?></td>
    </tr>
    <?php } ?>
</table>
</div>
