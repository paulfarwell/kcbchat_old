<?php

namespace common\modules\chat;

class Chat extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\chat\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
