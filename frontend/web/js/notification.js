
$( document ).ready(function() {
     var url = 'http://kcbchat.farwell-consultants.com';
     var socket = io.connect('http://kcbchat.farwell-consultants.com:443/socket',{ query: 'roomdata='+chatRoom.id+'' });

     if (chatRoom.host === chatRoom.chatUserId){
            chatRoom.role = "Ceo";
        }

     console.log(chatRoom);
    var activeChatArea = $("#area_active_chat");
    var activenotifarea = $("#notificationsBody");
    var activecounter = $("#notification_count");
    var chatForm = $("#chat-form");
    var tag = $("#message-tag");
    var messagetag =$("#message-hidden")
     var role = $("#role-hidden");
    var messageval = $("#message-field");
    var onlineUsersArea=$("#user_feed");
    var activeUsersArea=$("#user_feed_active");
    var onlineUsersStats=$("#user_stats");
    var mentions = $("#mention-hidden");
    var file_upload = $("#file-input");
    var filename_container = $("#filename-container");
    var likesHidden = $("#likes-hidden");
    messageval.keypress(function(e){

        if(e.which == 13) {
            e.preventDefault();
            chatForm.trigger('submit');
        }

    });

    chatForm.on('submit', function(e){
        e.preventDefault();
        var msgentered= messageval.val();
        var tagentered = messagetag.val();
         var rolentered = role.val();
        var mentionEntered = mentions.val();
        var file_uploadEntered = file_upload.val();
        var file = file_uploadEntered.replace(/.*[\/\\]/, '');
        var likentered = likesHidden.val();

        // console.log(likentered);


         var roles = rolentered.trim();
        var textmsgcontent = msgentered.trim();
        var textmsgtag = tagentered.trim();
        var mentionContent = mentionEntered.trim();
        var fileContent = file.trim();
        var likes = likentered.trim();

         if(likes.length > 0){
            // messageval = "Like";
            // // message = messageval.val();
            // console.log(messageval);
            // console.log(likes);
            
           socket.emit('newmessage', {msg:"Like", user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole:parseInt(role.val()), replyID: 0, mentionID: 0,likes:parseInt(likesHidden.val())});


        }
        else if(textmsgcontent.length == 0 ){
            return false;

        }else if(fileContent.length > 0){
         
        var ext = file.split(".").pop().toLowerCase();
        if($.inArray(ext, ["pdf"]) == -1) {
           alert("You can only upload PDF format");
        }else{
          $.ajax({
            type: "POST",
            url: "/site/fileupload",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(strData){
                var data = JSON.parse(strData);
            data.data.forEach(function(rsp){

                 socket.emit('newmessage', {msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole:parseInt(role.val()), replyID:0, mentionID: 0, upload:rsp.file, uploadType:rsp.filetype });

                  filename_container.empty();
                  messageval.val("");
                  file_upload.val(" ");


            });
            }
        });
        }


        }else if(textmsgtag.length > 0){
            
            // Create a new chat message and display it directly

        //showMessage("chatStarted");

        //createChatMessage(textarea.val(), 'local',name, img, moment());
        //scrollToBottom();
        scrollToChatBottom();

        // Send the message to the other person in the chat
        //console.log({msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole: chatRoom.role, replyID: parseInt(messagetag.val())})
        socket.emit('newmessage', {msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole:parseInt(role.val()), replyID: parseInt(messagetag.val()), mentionID: 0});


        // Empty the textarea
        messageval.val("");
        tag.text("");
        messagetag.val("");
        document.getElementById("message-tag").style.display="none";
        $(".emoji-wysiwyg-editor").text(" ");


    }else if (mentionContent.length > 0){

        
           // Create a new chat message and display it directly

        //showMessage("chatStarted");

        //createChatMessage(textarea.val(), 'local',name, img, moment());
        //scrollToBottom();
        scrollToChatBottom();

        // Send the message to the other person in the chat
        //console.log({msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole: chatRoom.role, replyID: parseInt(messagetag.val())})
        socket.emit('newmessage', {msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole:parseInt(role.val()), replyID:0 , mentionID: parseInt(mentions.val())});


        // Empty the textarea
        messageval.val("");
        mentions.val("");
        document.getElementById("mentions").style.display="none";
        $(".emoji-wysiwyg-editor").text(" ");
    } else if (textmsgtag.length == 0 && mentionContent == 0){
          

       //  // Create a new chat message and display it directly

       //  //showMessage("chatStarted");

       //  //createChatMessage(textarea.val(), 'local',name, img, moment());
       //  //scrollToBottom();
       //  scrollToChatBottom();

       //  // Send the message to the other person in the chat
       // // console.log({msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole: chatRoom.role, replyID: 3})
        socket.emit('newmessage', {msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole: parseInt(role.val()), replyID:0, mentionID: 0,upload:null, uploadType:null});

       //  // Empty the textarea
         messageval.val("");
         $(".emoji-wysiwyg-editor").text(" ");
   }
});


    var oldMsgButton=$("#loadOldMsgs");
    var channel = 'notification';
     console.log(channel);

    socket.on('connect', function(){
        console.log('connect')
        socket.emit('room', {room: chatRoom.id, maxUsers: chatRoom.maxUsers, userID: chatRoom.chatUserId});
        socket.emit('login', {key: chatRoom.chatKey, id: chatRoom.id, maxUsers: chatRoom.maxUsers, avatarpath: chatRoom.avatarpath});
    });

    socket.on(channel, function (data) {

        var message = JSON.parse(data);
        createChatMessage (message.id, message.message, message.chatUserName, message.avatar, moment(), message.role);
    });
    socket.on('receive', function(data){
     //console.log(data)

            // // //createChatMessage (message.id, message.message, message.chatUserName, message.avatar, moment(), message.role);
              createChatMessage( data.msgid , data.msg, data.user, data.avatar , moment(),data.userRole,chatRoom.host,chatRoom.hostname, data.reply, data.replyUser, data.replyMsg ,data.likes,data.attachmentID,0);
              notifyuser( data.msgid , data.msg, data.user, data.avatar , moment(),data.userRole,data.mentionID, data.mentionName, data.mentionLastName);

            // //     // //scrollToBottom();
            // //    // scrollToChatBottom();



        });

    socket.on('roommates',function(data){
        data.forEach(function(user){
            //console.log(user);
            createNewUser(user.userid, user.Name , user.avatar, user.activeStatus );
        });
    });

    socket.on('newuser',function(data){
        //console.log(data);
        createNewUser(data.userid, data.Name ,data.avatar, data.activeStatus);
    });

    socket.on('onlineusers',function(data){
        //console.log(data);
        var onlineusers = data + " USERS ONLINE";
        //console.log(onlineusers);
        onlineUsersStats.html(onlineusers);
        //createNewUser(data.userid, data.Name ,data.avatar);
    });



    oldMsgButton.click(function(e){
        // console.log(chatRoom);
        socket.emit('receiveOldMsg', {chatRoom});
    });


socket.on("test", function(data) {
    console.log(data)
});

    socket.on('oldMsgs',function(data){
          console.log(data);
         var msgdata = data.theMsgs;
         var msglike = data.like;
         msgdata.forEach(function(msg){
         	var msgLikes = [];
         	msglike.forEach(function(like){
              if(like.MessageID === msg.ID){
              	console.log(msg.ID,like.Name);
	    		msgLikes.push(like.Name)
               }
         	});
 	       if(msg.replyID >0){
           msgdata.forEach(function(testRepMsg){
             if(testRepMsg.ID === msg.replyID){
              createChatMessage (msg.ID, msg.MessageText, msg.UserName, msg.avatar, moment(msg.ChatTime), parseInt(msg.role),chatRoom.host,chatRoom.hostname, testRepMsg.replyID,testRepMsg.UserName,testRepMsg.MessageText,undefined,undefined,undefined);
             }
           });

      	 }else{

               createChatMessage (msg.ID, msg.MessageText, msg.UserName, msg.avatar, moment(msg.ChatTime), parseInt(msg.role),chatRoom.host,chatRoom.hostname,undefined ,undefined ,undefined ,msg.likes,msg.attachment,msgLikes);
      	 }


           });
         oldMsgButton.hide();
         scrollToChatBottom();

    });

    socket.on('likemsgid',function(data){
          console.log(data)
          if(data.liked > 0){
              console.log('You can only like once');
            }
        msgliked(data);
        createChatMessage (0, 0, 0, 0,  moment(msg.ChatTime), 0,0,0, 0,0,0,0,0,data.likingUser);

    });

    socket.on('searchResult',function(data){
        console.log(data);
    });




    function notifyuser(msgid, msg , user, img, now, senderrole, mentionID, mentionName, mentionLastName){

        var numMessages = $('div.notification_single').length + 1;
        //message = formatmessage(msg,msgtype,mimetype,Filename);

       var note = '<div class="notification_single active" id="active_notification'+ msgid +'">'+
            '<div class="notification-title text-info">';

         if(mentionID == chatRoom.chatUserId ){
            note += '<span class="notification-title-span"><p>You Have a Mention From</p>'+ user + ' </span> ';
         }else{
            note += '<span class="notification-title-span">'+ user + ' </span> ';
         }

        var chattime = now.format("h:mm:ss a");
        var notification = $(
            note += '</div>'+
            '<div class="notification-description">'+
            '<span class="notification-description-span">'+ msg + ' </span> ' +
            '</div>'+
            '<div class="notification-ago">'+
            '<span class="notification-ago-span">'+ chattime + ' </span> ' +
            '</div>'+
            '</div>'
            );
        if (user != chatRoom.chatUserName ) {
            $( "#notificationsBody" ).last().removeClass( "notification_seen" );
            activenotifarea.append(notification);
            activecounter.html( numMessages );
            $("#notification_count").show();
        }
    }

    // function ReplyTo(reply){
    //     console.log(reply)
    //     console.log(chatRoom)
    //   socket.emit('ReplyTo',{reply,chatRoom});
    // }

    function createChatMessage(msgid, msg , user, avatar, now, senderrole, host,hostname, reply,replyUser,replyMsg,likes,attachmentID,likingUser){
         console.log(senderrole);
        
    if (typeof senderrole === 'string') {
         userRole = senderrole;

     }else if(senderrole === 2){
        userRole = 'ceo';

    }else if(senderrole === 3){
        userRole = 'ADMIN';
    }else{
        userRole = 'user';
    }
    var chattime = now.format("h:mm:ss a");
    var userClass='you';

    likes = typeof likes !== 'undefined' ? likes : 0;

    if(user==chatRoom.chatUserName){
        userClass='me';
    }
        // if(reply > 0){
        //    ReplyTo(reply);
        // }
        // if(likingUser === undefined){
        //   var likingUser="";
        // }else{
        // var likingUser= likingUser.join('<br>');
        // }
        likingUser = typeof likingUser !== 'undefined' ? likingUser : '';



        var message = '<div class="per-chat '+ userRole.toLowerCase() + ' ' + userClass + '" id="active_msg_'+ msgid +'">'+
            '<div class="col-xs-3 col-sm-3 col-md-1 user-name">'+'<span class="user_image online-user-avatar">'
         if (avatar === null){
           message+= '<img class="img-circle" src="'+url+'/images/avatar/user.jpg" alt="'+name+'">'+ ' </span> ';
         }else{
            message+= '<img class="img-circle" src="'+url+'/images/avatar/'+avatar+'" alt="'+name+'">'+ ' </span> ';
         }

            message +='</div>'+
            '<div class="col-xs-9 col-sm-9 col-md-10 posted-chat message_content">'+
            '<div class="msg_id" style="display:none">'+ msgid +'</div><span  class="conversation col-sm-10 col-xs-9">';
        //Add reply message
          if(typeof replyMsg != 'undefined'){
            message += '<div class="reply"><div class="reply_originator">'+ replyUser +'</div>'+ replyMsg +'</div>';
           }
            if(attachmentID != null){
                message +='<div class="reply"><a href="/site/attachment?id='+attachmentID+'">View attachment</a></div>';
          }

            if(senderrole === 1){
                message +='<div class="user_originator">'+ user + ' </div>';

            }else if(senderrole ===2){

                message +='<div class="ceo_originator">'+ user + ' </div>';
            }else{
              message+='<div class="admin_originator">'+ user + ' </div>';
            }
          message += ' <span class="conversation_msg">' + youtube(msg) + '</span><span class="msg_time">' + chattime + ' </span></span>';



        var adminview = $(

            message+'<span class="col-xs-3 col-sm-2 posted-chat-actions  msgtions">'+
            '<span><input type="checkbox" data-toggle="tooltip" data-placement="right" title="Filter Message" class="filter_msg axn hidden-xs" onclick="filterThisMsg('+ msgid +');"  /></span>'+
            '<span style="display:inline-flex" class="rep"><button type="button" data-toggle="tooltip" data-placement="right" title="Reply Message" class="qoute_msg reply-icon axn" onclick="qouteThisMsg('+ msgid +');"  /></span>'+
            '<span class="tooltip likeclass" style="display:inline-flex">'+
             '<span class="tooltiptext">'+likingUser +'</span>'+
                '<button type="button" id="likeButton" class="like_msg like-icon axn" onclick="likeThisMsg('+ msgid +');"/></button>'+
                '<span class="likecounter" id="msg_like_'+msgid+'">'+likes+'</span>'+
            '</span>'+
            '</span>'+
            '</div>'+
            '</div>'
            );
        var hostview = $(

          
            message+'<span class="col-xs-3 col-sm-2 posted-chat-actions  msgtions">'+
            '<span><input type="checkbox" data-toggle="tooltip" data-placement="right" title="Filter Message" class="filter_msg axn hidden-xs" onclick="filterThisMsg('+ msgid +');"  /></span>'+
            '<span style="display:inline-flex" class="rep"><button type="button" data-toggle="tooltip" data-placement="right" title="Reply Message" class="qoute_msg reply-icon axn" onclick="qouteThisMsg('+ msgid +');"  /></span>'+
            '<span class="tooltip likeclass" style="display:inline-flex">'+
             '<span class="tooltiptext">'+likingUser +'</span>'+
                '<button type="button" id="likeButton" class="like_msg like-icon axn" onclick="likeThisMsg('+ msgid +');"/></button>'+
                '<span class="likecounter" id="msg_like_'+msgid+'">'+likes+'</span>'+
            '</span>'+
            '</span>'+
            '</div>'+
            '</div>'
            );
        var userview = $(
            message +
            '<span class="col-xs-3 col-sm-2 posted-chat-actions  msg_actions">'+
            '<span><button type="button" data-toggle="tooltip" data-placement="right" title="Reply Message" class="qoute_msg reply-icon axn" onclick="qouteThisMsg('+ msgid +');" /></span>'+
            // '<button type="button" data-toggle="tooltip" data-placement="right" title="Tweet Message" class="tweet_msg axn" onclick="tweetThisMsg('+ msgid +');"  /></button>'+
             '<span class="tooltip likeclass"  style=" display:inline-flex">'+
             '<span class="tooltiptext">'+likingUser+'<br/>' +'</span>'+
                '<button type="button" id="likeButton" class="like_msg like-icon axn" onclick="likeThisMsg('+ msgid +');"/></button>'+
                '<span class="likecounter" id="msg_like_'+msgid+'">'+likes+'</span>'+
            '</span>'+
            
            '</span>'+

            '</div>'+
            '</div>'
            );
        //console.log(chatRoom.chatUserId);

        if(chatRoom.role ==='USER'){

         activeChatArea.append(userview);

        }else if(chatRoom.role ==='ADMIN'){

            activeChatArea.append(adminview);
        }
        else{
            activeChatArea.append(hostview);
        }


    }

    // function likeThisMsg(msgid){
    //     socket.emit('likemsg', {msgid: msgid});
    //     //console.log("liking msg");
    // }

    function createNewUser(userid ,name , avatar, activestatus ){
        obj = Object.values(userid).length;
        //console.log(obj)
        var liID ='user_'+userid;
        var user = $('<tr class="activeuser online_user '+ userid + '" id="user_'+ userid +'" data-content="user_'+ name +'">'+
         '<td class="online-user-info-circle"><i class="fa fa-circle"></i></td>'+
         '<td class="user_image online-user-avatar">'+
         '<img class="img-circle" src="'+avatar+'" alt="'+name+'">'+
         '</td>'+
         '<td class="user_name online-user-info">'+
         '<span class="avatar-bio">' + name + '</span>'+
         '</td>'+
         '</tr>'
         );
        if (activestatus == true ) {
            var liaID ='usera_'+userid;
            var usera = $(
                '<tr class="online_user '+ userid + '" id="usera_'+ userid +'" data-content="user_'+ name +'">'+
                '<td class="online-user-info-circle"><i class="fa fa-circle"></i></td>'+
                '<td class="user_image online-user-avatar">'+
                '<img class="img-circle" src="'+avatar+'" alt="'+name+'">'+
                '</td>'+
                '<td class="user_name online-user-info">'+
                '<span class="avatar-bio">' + name + '</span>'+
                '</td>'+
                '</tr>'+
                '</tr>'
                );
            //console.log($("#" + liaID).length);
            if($("#" + liaID).length == 0) {

                activeUsersArea.prepend(usera);
            }
        }
        if($("#" + liID).length == 0) {
            onlineUsersArea.prepend(user);
        }

    }
    // $('#user_feed').on('mouseover mouseout', '.online_user', function() {
    //  var classname = $(this).attr('class');
    //  var res = "." + classname.replace(/\s/g, ".");
    //  $(res).popup('show',{
    //      popup:res
    //  });

    //  // console.log(this);
    //  console.log('the res  ', res);
    // });

/*
    function createNewUser(user){
        console.log(user);
        var liID='user_'+user.userid;

        var li=$(
            '<li class="activeuser '+user.userid +'" id="user_'+user.userid +'">'+
            '<p class="online_users_chatting">'+ user.Name +'</p>'+
            '<p class="clear"></p>'+
            '<li>'

        );
        if(user.activeStatus==1){
            var liaID='usera_'+user.userid;
            var lia=$(
                '<li class="onlineuser '+user.userid +'" id="usera_'+user.userid +'">'+
                '<p class="online_users_chatting">'+ user.Name +'</p>'+
                '<p class="clear"></p>'+
                '<li>'
            );
            if($("#" + liaID).length == 0) {
                //activeUsersArea
             activeUsersArea.prepend(lia);
            }
        }
        if($("#" + liID).length == 0) {
          onlineUsersArea.prepend(li);
        }
    }
    */

});




function scrollToChatBottom() {
    $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight}, 1000);
};

function filterThisMsg(msgid) {
    if(chatRoom.role!='USER'){
        var theLi='active_msg_'+msgid;
        var liTofilter=$("#"+theLi);
        var newLiInFilter;
        var newLiInFilterID='filtered_msg_'+msgid;
        newLiInFilter=$(liTofilter).clone().prop({ id: newLiInFilterID});
        $('#area_filtered_chat').append(newLiInFilter);
        liTofilter.hide();
        //get the ccheckbok
        $("#"+newLiInFilterID+" .filter_msg").prop('checked', false);
        $("#"+newLiInFilterID+" .filter_msg").attr("onclick","unfilterThisMsg("+msgid+")");
    }
}
function unfilterThisMsg(msgid){
    var theLi='filtered_msg_'+msgid;
    var liToUnfilter=$("#"+theLi);
    liToUnfilter.remove();
    var liToactivate=$('#active_msg_'+msgid);
    liToactivate.show();
    $("#active_msg_"+msgid+" .filter_msg").prop('checked', false);
}
function qouteThisMsg(msgid){

    var myuser =$('#active_msg_'+msgid+' .msg_originator').text();
    var mytext =$('#active_msg_'+msgid+' .conversation_msg').text();
    var myid =$('#active_msg_'+msgid+' .msg_id').text();
    // console.log(myid);
    var editedtext='@'+myuser +'"' +mytext+'"';
    document.getElementById("message-tag").style.display="block";
    $('#message-tag').text(editedtext);
    $('#message-hidden').val(myid);




}


function youtube($string)
{
   if ($string) {
    return $string.replace(
      /(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g,
      '<iframe width="83%" height="293" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>')
    .replace(
        /(?:http:\/\/)?(?:www\.)?(?:vimeo\.com)\/(.+)/g,
        '<iframe src="//player.vimeo.com/video/$1" width="200" height="100" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
        );
}
}

function msgliked(data){
    console.log('msgliked');
    var msgspanselector= 'span#msg_like_'+data.likedmsgid;
    $(msgspanselector).text(data.likedmsgcount);


}

function likeThisMsg(msgid){
    var likeHidden = $("#likes-hidden");
    likeHidden.val(msgid);
   document.getElementById("kcbsendbutton").click();
}

  $( function() {
    $( document ).tooltip();
  } );
