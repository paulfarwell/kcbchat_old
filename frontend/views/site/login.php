<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */

$settings = Yii::$app->settings;
// $settings->clearCache();
$logininfo = $settings->get('Theme.loginDescription');

?>
<?= $this->render('@frontend/views/templates/metro_register') ?>

<section class=" chat-details-container container-orange">

    <div class="container section-login"> 

        <div class="row">

            <div class="col-md-6 col-xs-12 col-sm-12 col-lg-6 col-md-offset-3 col-lg-offset-3" id="login">

                <div class="login-form form-black" id="login_section" style="padding-top: 20px">
                    <?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
                                
                    <h1 style="padding-left:20px;color:#064367">Already registered,login & join Chatroom</h1>
                     <p> Please use your Bank Login details</p>
                    <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>

                     <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>' eg john@kcbgroup.com']) ?>

                     <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Bank login password']) ?>


                     <div class="clearfix"> </div>
                    
                    <div class="col-tight-left col-sm-6">
                        <div class="pull-left">
                
                        
                    <?= Html::submitButton(Yii::t('user', 'join Chatroom'), ['class' => 'btn btn-join join-chat', 'tabindex' => '3']) ?>
                       </div>
                    </div>

                    <div class="col-sm-6">
                       
                    </div>
                    <div class="clearfix"> </div>

                    <?php ActiveForm::end(); ?>
                        
                </div> <!-- login_section -->


             </div><!-- col-md-6 -->


        </div><!-- main row -->

    </div> <!-- main container -->    
</section> <!-- container-section -->