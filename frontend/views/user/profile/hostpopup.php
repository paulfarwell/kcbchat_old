<?php
/**
 * var $model  user model
 * var $profile the user profile model
 */
 //print_r($model);
use Zelenin\yii\SemanticUI\Elements;
use yii\helpers\Html;
use backend\models\Host
?>

<div class="profpopupholder">
	<div class="top">
		<span class="make-responsive">
            <?php if(Host::findIdentity($model['Host'])->profile->avatar):?>
                <div class="image_holder">
                	<?= Html::img('@web/images/avatar/'.Host::findIdentity($model['chatHost'])->profile->avatar, ['class' => 'archive_host_image img-responsive']) ?> 
                	
                  </div>
            <?php else:?>
              <div class="image_holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>
            </span>
		<!-- <div class="pic">
			<?= Elements::image($model->profile->getImgSrc('avatar'), ['width' => '200px']) ?>
		</div> --><!--end pic-->
		<div class="details">
			<table class="detailstable">
				<tr>
					<td class="ceo-name"><?= Host::findIdentity($model['Host'])->profile->name ?> <?= Host::findIdentity($model['Host'])->profile->lastname ?></td>
				</tr>
				<tr>
					<td><?php echo Host::findIdentity($model['Host'])->profile->designation ?></td>
				</tr>
				<tr>
					<td><?php echo Host::findIdentity($model['Host'])->profile->organization ?></td>
				</tr>
				<!--
				<tr>
					<td>
						<?php # if($profile->twitter): ?>
						<a href="https://twitter.com/<?php # echo $profile -> twitter; ?>" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow CEo</a>
						<?php # endif; ?>
					</td>
				</tr>	-->											
			</table>
			
		</div> <!--end details-->
		<div class="clearfix"></div>
	</div><!--end top-->
	<div class="bio">
		<?php echo Host::findIdentity($model['Host'])->profile->bio ?>
	</div><!--end end bio-->
</div>