<?php 
use yii\helpers\Url;
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <div class="container">
      <div class="navbar-header">
        <h3 class="nav-brand" style="text-transform:initial;color:#95C13D">Reviews and Feedback</h3>
      
    </div>
     <!--  <ul class="nav navbar-nav welcome-nav "><li><h3 style="color:#95C13D; text-transform: initial;">Welcome To The Chat Room</h3></li></ul> -->
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
      <li class="help-nav"><a href="<?= Url::to(['/site/help'])?>"><h3><span class="fa fa-question fa-1x " style="color:#95C13D"></span><b>Help</b></h3></a></li>
        <li ><a href="<?= Url::to(['/site/logout'])?>" data-method="post" > < Logout</a></li>
      </ul>
    </li>
      </ul>
       </div>
    </div>
  </div>
</nav>