<?php
namespace common\models;
 
use Yii;
use common\models\Chat;
use backend\models\ReviewQuestions;
 
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review_answers';
    }
     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['QuestionsID','UserID','Answer_1'], 'required'],
            [['Answer_1', 'Answer_2','Answer_3','Answer_4','Answer_5'], 'string', 'max' => 255]
            
          
        ];
    }

    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['ID' => 'Chat']);
    }

     public function getQuestions()
    {
        return $this->hasOne(ReviewQuestions::className(), ['ID' => 'QuestionsID']);
    }
 
}