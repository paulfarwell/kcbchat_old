<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Chat;

use Zelenin\yii\SemanticUI\modules\Modal;
$settings = Yii::$app->settings;
$settings->clearCache();
$value = $settings->get('Theme.homePageDescription');
// Automatically called on set();
?>

<!-- home-jumbo -->
<header>
    <div class="container-fluid">
        <div class="intro-text">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              
            
            <div class=" carousel-body events-slider carousel-section container-fluid col-xs-12 col-sm-4 col-md-4 col-lg-6">
    <h4 class="carousel-section-head"><b>UPCOMING CHAT DETAILS:</b></h4>
    <div class="event-slider-content" id="events_section">
        <?php foreach ($chats as $chat): ?>
           <table class="table-slide">
                <tr> <td><h4 style="text-transform:initial; "><b>Topic:</b></h4></td> <td><h4 style="text-transform:initial; "><?php echo $chat->Topic;?></h4></td></tr>
                <tr> <td><h4 style="text-transform:initial; "><b>Date:</b></h4></td> <td><h4 style="text-transform:initial; "><?php echo $chat->ChatDate;?></h4></td></tr>
                <tr> <td><h4 style="text-transform:initial; "><b>Time:</b></h4></td> <td><h4 style="text-transform:initial; "><?= Html::encode(Yii::$app->formatter->asTime($chat->StartTime, "HH:mm a")." - ".Yii::$app->formatter->asTime($chat->EndTime, "HH:mm a")); ?></h4></td></tr>
                <tr> <td><h4 style="text-transform:initial; "><b>Host:</b></h4></td> <td><h4 style="text-transform:initial; "><?php echo $chat->ceostring;?></h4></tr>
                <tr><td><?= Html::a('Topic Details', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->Topic . "Details",  'class' => "btn btn-custom",]) ?></td><td class="jumbo-b">  <?= Html::a('About The Host', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->ceostring . "Details",  'class' => "btn btn-custom", 'style'=>'margin-left:5px']) ?></td></tr>
            </table>
        <?php endforeach;?>    
    </div>
</div>
            <ul class="pull-right kcb-menu col-xs-12 col-sm-4 col-md-4 col-lg-6">
               <?php if (Yii::$app->user->isGuest):?>
           <!--  <?= Html::img('@web/images/icons/front/register.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li> <a href="<?= Url::toRoute(['site/register#register',])?>" >REGISTER</a><p> and create an account</p></li><hr> -->
            <?php else:?>
                <?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li><a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>and share your with the team</p> </li><hr>
            <?php endif;?>
             <?php if (Yii::$app->user->isGuest):?>
                <?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li><a href="<?= Url::toRoute(['site/login'])?>" >LOGIN</a><p> and join the conversation</p></li><hr>
            <?php else:?>
                  <?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-10px'])?><li><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >LOGOUT</a></li><hr>
                   <i class="fa fa-user "></i><li ><a href="<?= Url::to(['/user/settings/myprofile'])?>" data-method="post" >MY PROFILE</a></li><hr>
            <?php endif;?>
            <?php if (Yii::$app->user->isGuest):?>
                 <?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li><a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>and share your with the team</p> </li><hr>
               <?= Html::img('@web/images/icons/front/help.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex'])?><a href="<?= Url::to(['/site/help'])?>" >Help</a></li>
           <?php else:?>
             <?= Html::img('@web/images/icons/front/help.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex'])?><a href="<?= Url::to(['/site/help'])?>" >Help</a></li>
           <?php endif;?>
            </ul> 
  </div>
           
           </div>
            <!-- <div class="intro-heading">Welcome To Our Studio!</div> -->
            </div>


            <!-- <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a> -->
      </div>
</header>
<?= $this->render('@frontend/views/templates/metro') ?>
             

                
<!--  -->
<!-- ends home-jumbo -->