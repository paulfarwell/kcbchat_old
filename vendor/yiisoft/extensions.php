<?php

$vendorDir = dirname(__DIR__);

return array (
  '2amigos/yii2-ckeditor-widget' => 
  array (
    'name' => '2amigos/yii2-ckeditor-widget',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@dosamigos/ckeditor' => $vendorDir . '/2amigos/yii2-ckeditor-widget/src',
    ),
  ),
  '2amigos/yii2-date-time-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-time-picker-widget',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@dosamigos/datetimepicker' => $vendorDir . '/2amigos/yii2-date-time-picker-widget/src',
    ),
  ),
  'alexandernst/yii2-device-detect' => 
  array (
    'name' => 'alexandernst/yii2-device-detect',
    'version' => '0.0.10.0',
    'alias' => 
    array (
      '@alexandernst/devicedetect' => $vendorDir . '/alexandernst/yii2-device-detect',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.10.3.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'dektrium/yii2-rbac' => 
  array (
    'name' => 'dektrium/yii2-rbac',
    'version' => '1.0.0.0-alpha',
    'alias' => 
    array (
      '@dektrium/rbac' => $vendorDir . '/dektrium/yii2-rbac',
    ),
    'bootstrap' => 'dektrium\\rbac\\Bootstrap',
  ),
  'kop/yii2-scroll-pager' => 
  array (
    'name' => 'kop/yii2-scroll-pager',
    'version' => '2.5.0.0',
    'alias' => 
    array (
      '@kop/y2sp' => $vendorDir . '/kop/yii2-scroll-pager',
    ),
  ),
  'pheme/yii2-settings' => 
  array (
    'name' => 'pheme/yii2-settings',
    'version' => '0.5.0.0',
    'alias' => 
    array (
      '@pheme/settings' => $vendorDir . '/pheme/yii2-settings',
    ),
  ),
  'edvlerblog/yii2-adldap-module' => 
  array (
    'name' => 'edvlerblog/yii2-adldap-module',
    'version' => '4.0.0.0',
    'alias' => 
    array (
      '@Edvlerblog/Adldap2' => $vendorDir . '/edvlerblog/yii2-adldap-module/src',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '0.9.12.0',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
  '2amigos/yii2-date-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-picker-widget',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@dosamigos/datepicker' => $vendorDir . '/2amigos/yii2-date-picker-widget/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.9.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'sjaakp/yii2-illustrated-behavior' => 
  array (
    'name' => 'sjaakp/yii2-illustrated-behavior',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@sjaakp/illustrated' => $vendorDir . '/sjaakp/yii2-illustrated-behavior',
    ),
  ),
  'zelenin/yii2-semantic-ui' => 
  array (
    'name' => 'zelenin/yii2-semantic-ui',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@Zelenin/yii/SemanticUI' => $vendorDir . '/zelenin/yii2-semantic-ui',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.12.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'pheme/yii2-toggle-column' => 
  array (
    'name' => 'pheme/yii2-toggle-column',
    'version' => '0.7.0.0',
    'alias' => 
    array (
      '@pheme/grid' => $vendorDir . '/pheme/yii2-toggle-column',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.4.9.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.1.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-dynagrid' => 
  array (
    'name' => 'kartik-v/yii2-dynagrid',
    'version' => '1.4.8.0',
    'alias' => 
    array (
      '@kartik/dynagrid' => $vendorDir . '/kartik-v/yii2-dynagrid',
    ),
  ),
  'kartik-v/yii2-export' => 
  array (
    'name' => 'kartik-v/yii2-export',
    'version' => '1.2.6.0',
    'alias' => 
    array (
      '@kartik/export' => $vendorDir . '/kartik-v/yii2-export',
    ),
  ),
);
