<?php
namespace backend\controllers;

use Yii;
use pheme\settings\models\Setting;
use pheme\settings\models\SettingSearch;
use pheme\grid\actions\ToggleAction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Theme;
use yii\web\UploadedFile;


class ThemeController extends Controller
{
	public $scenario;
    public  $defaultAction = 'create';

    

    public function actionCreate()
    {
        /* @var $model \yii\db\ActiveRecord */
        $fileName = 'file';
        $uploadPath = \Yii::getAlias('@frontend/web/images/site/');
        $model = new Theme();
        if ($this->scenario) {
            $model->setScenario($this->scenario);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $logo = UploadedFile::getInstance($model, 'logo');
            if (!empty($logo)) {
                $logoname =  uniqid() . preg_replace('/\s+/', '_', $logo->name);
                if ($logo->saveAs($uploadPath . '/' . $logoname)) {
                    $model->logo= $logoname;
                }
            }
            $banner = UploadedFile::getInstance($model, 'banner');
            if (!empty($banner)) {

                $bannername = uniqid() . preg_replace('/\s+/', '_', $banner->name);

                if ($banner->saveAs($uploadPath . '/' . $bannername)) {
                    $model->banner= $bannername;
                }
            }

            foreach ($model->toArray() as $key => $value) {
                if (!empty($value)) {
                    Yii::$app->settings->set($key, $value, $model->formName());
                }
  
            }
            Yii::$app->getSession()->addFlash('success','Successfully saved settings on theme');
        }
        foreach ($model->attributes() as $key) {
            $model->{$key} = Yii::$app->settings->get($key, $model->formName());
        }
        return $this->render('create', [
                'model' => $model,
       ]);

    }


    public function beforeAction($action)
    {
        Yii::$app->i18n->translations['extensions/yii2-settings/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => '@vendor/pheme/yii2-settings/messages',
            'fileMap' => [
                'extensions/yii2-settings/settings' => 'settings.php',
            ],
        ];
        return parent::beforeAction($action);
    }

}