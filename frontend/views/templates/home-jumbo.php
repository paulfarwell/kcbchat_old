<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Chat;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\Elements;
$settings = Yii::$app->settings;
$settings->clearCache();
$value = $settings->get('Theme.homePageDescription');
// Automatically called on set();
?>
<style>
@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
   .ie10up{
      .help{
        
      }

   }
}
</style>
<!-- home-jumbo -->
<div class="background-over">
<?php if (!empty($banners->chat_banner)):?>
<header class="overlay" style="background-image: url('chatBanners/<?=$banners->chat_banner?>'); background-blend-mode: overlay;
background-color: rgb(0,45,63,0.3);" >
<?php else:?>
<header class="overlay" style="background-image: url('images/site/home_featured.jpg'); background-blend-mode: overlay;
background-color: rgb(0,45,63,0.3);" >
<?php endif;?>
    <div class="container-fluid">
        <div class="intro-text">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <!-- slider-chat-events -->
<div class=" carousel-body events-slider carousel-section container-fluid">
    <h3 class="carousel-section-head"><b>UPCOMING CHAT DETAILS:</b></h3>
    <div class="event-slider-content" id="events_section">
        <?php foreach ($chats as $chat): ?>
           <table class="table-slide">

                <tr> <td><h3 style="text-transform:initial; "><b>Topic:</b></h3></td> <td><h3 style="text-transform:initial; "><?php echo $chat->Topic;?></h3></td></tr>
                <tr> <td><h3 style="text-transform:initial; "><b>Date:</b></h3></td> <td><h3 style="text-transform:initial; "><?php echo $chat->ChatDate;?></h3></td></tr>
                <tr> <td><h3 style="text-transform:initial; "><b>Time:</b></h3></td> <td><h3 style="text-transform:initial; "><?= Html::encode(Yii::$app->formatter->asTime($chat->StartTime, "HH:mm a")." - ".Yii::$app->formatter->asTime($chat->EndTime, "HH:mm a")); ?></h3></td></tr>
                <tr> <td><h3 style="text-transform:initial; "><b>Host:</b></h3></td> <td><h3 style="text-transform:initial; "><?php if(empty($chat->ceostring)){ echo $chat->hoststring;}else{echo $chat->ceostring;}?></h2></h3></tr>
                <tr><td></td><td><?= Html::a('Topic Details', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->Topic . "Details",  'class' => "btn btn-custom",]) ?>  <?= Html::a('About The Host', Url::toRoute(['chat/details', 'id' => $chat->ID]), ['title' => $chat->ceostring . "Details",  'class' => "btn btn-custom", 'style'=>'margin-left:5px']) ?></td></tr>
            </table>
        <?php endforeach;?>
    </div>
</div>
<!-- slider-chat-events -->
                </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 ">
            <ul class="pull-right kcb-menu">
               <?php if (Yii::$app->user->isGuest):?>
           <!--  <?= Html::img('@web/images/icons/front/register.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li> <a href="<?= Url::toRoute(['site/register#register',])?>" >REGISTER</a><p> and create an account</p></li><hr> -->
            <?php else:?>
                <li><?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?></li><li><a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>and share your insights with the team</p> </li><hr>
            <?php endif;?>
             <?php if (Yii::$app->user->isGuest):?>
                <li><?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?><li><a href="<?= Url::toRoute(['site/login'])?>" >LOGIN</a><p> and join the conversation</p></li><hr>
            <?php else:?>
                  <?= Html::img('@web/images/icons/front/lock.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-10px'])?><li><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >LOGOUT</a></li><hr>
                   <i class="fa fa-user "></i><li ><a href="<?= Url::to(['/user/settings/myprofile'])?>" data-method="post" >MY PROFILE</a></li><hr>
            <?php endif;?>
             <?php if (Yii::$app->user->isGuest):?>
                 <li><?= Html::img('@web/images/icons/front/chat.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex;margin-top:-40px'])?></li><li><a href="<?= Url::toRoute(['chat/enter'])?>">ENTER CHAT ROOM</a><p>and share your insights with the team</p> </li><hr>
               <li><?= Html::img('@web/images/icons/front/help.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex'])?></li><li><a href="<?= Url::to(['/site/help'],['class'=>'help'])?>" >Help</a></li><hr>
           <?php else:?>
             <li><?= Html::img('@web/images/icons/front/help.png',['class' => 'img-responsive','style'=>'width:40px; height:40px; display:inline-flex'])?></li><li><a href="<?= Url::to(['/site/help'],['class'=>'help'])?>" >Help</a></li>
           <?php endif;?>
            </ul>
            </div>


            <!-- <div class="intro-heading">Welcome To Our Studio!</div> -->
            </div>


            <!-- <a href="#services" class="page-scroll btn btn-xl">Tell Me More</a> -->
        </div>
</header>
</div>
<?= $this->render('@frontend/views/templates/metro') ?>



<!--  -->
<!-- ends home-jumbo -->
