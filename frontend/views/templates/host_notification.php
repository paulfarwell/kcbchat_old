<?php
$this->registerJsFile('/js/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/host-notification-helper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
use Zelenin\yii\SemanticUI\Elements;
use yii\helpers\Html;
?>

<?php if($model->profile == null):?>
<div class="notifications-wrapper">
	
	<ul id="nav" class="notifications_ul">
		<li id="host-notification_li" class="pull-right">
		<button class="ui info_launcher host-button  button"  id="host-notificationLink">
		   <?php echo Html::a(Html::img('@web/images/icons/host.png', ['class' => ' img-responsive'])); ?>
		</button>
		
	<!--
		<a href="#" id="notificationLink"><span id="notification_count">3</span><i class="nav-icon fa fa-bullhorn"></i> 
		<span class="small-screen-text">Notifications</span></a>-->

		<div id="host-notificationContainer">
		<!--<div id="notificationTitle">New Messages</div>-->
		<div class="host-notificationContainer">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
					<?= Html::img('@web/images/icons/close.png',['class' => 'img-responsive', 'id' =>'my_host_close'])?>
				</div>
			<span class="make-responsive">
            
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            
            </span>

            <h2><?= ucfirst($model->first_name) ?> <?= ucfirst($model->last_name) ?></h2>

            <p>
		       
	           </p>
            </div>

		
		<!--<div id="notificationFooter"><a href="#">See All</a></div>-->
		</div>

		</li>
	</ul>
</div>


<?php else:?>	
<div class="notifications-wrapper">
	
	<ul id="nav" class="notifications_ul">
		<li id="host-notification_li" class="pull-right">
		<button class="ui info_launcher host-button  button"  id="host-notificationLink">
		   <?php echo Html::a(Html::img('@web/images/icons/host.png', ['class' => ' img-responsive'])); ?>
		</button>
		
	<!--
		<a href="#" id="notificationLink"><span id="notification_count">3</span><i class="nav-icon fa fa-bullhorn"></i> 
		<span class="small-screen-text">Notifications</span></a>-->

		<div id="host-notificationContainer">
		<!--<div id="notificationTitle">New Messages</div>-->
		<div class="host-notificationContainer">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
					<?= Html::img('@web/images/icons/close.png',['class' => 'img-responsive', 'id' =>'my_host_close'])?>
				</div>
			<span class="make-responsive">
            <?php if($model->profile->avatar):?>
                <div class="host-avatar-holder">
                <?= Html::img('@web/images/avatar/'.$model->profile->avatar, ['class' => 'img-responsive']); ?>
                  </div>
            <?php else:?>
              <div class="host-avatar-holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-responsive'])?>
                </div>
            <?php endif;?>
            </span>

            <h2><?= ucfirst($model->profile->name) ?> <?= ucfirst($model->profile->lastname) ?></h2>

            <p>
		         <?php echo $model->profile->bio ?>
	           </p>
            </div>

		
		<!--<div id="notificationFooter"><a href="#">See All</a></div>-->
		</div>

		</li>
	</ul>
</div>
<?php endif;?>