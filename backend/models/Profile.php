<?php 

namespace backend\models;

use Yii;
use sjaakp\illustrated\Illustrated;
use common\models\Countries;
use dektrium\user\models\Profile as BaseProfile;


class Profile extends BaseProfile
{
    const USER_USER 	= 1;
    const USER_CEO  	= 2;
    const USER_ADMIN 	= 3;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            "class" => Illustrated::className(),
            'attributes' => [
                    'avatar' => [
                        'aspectRatio' => 1,
                        //allow for crop steps to factor in larger requirements
                        'cropSize' => 200,
                        'tooSmallMsg'=> 'The Image uploaded "%s" is too small (%d×%d) to crop or scale to an appropriate profile picture.'
                    ],     
                ],
            'directory' => '@images',
            'baseUrl' => '@imagesurl',
            'noImage'=>'Not a valid image',
            'fileValidation'=>[
                'extensions' => ['png', 'jpg', 'gif'], 
                'maxSize' => 1024*1024,
            ],
           
        ];

        return $behaviors;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'role,lastname, avatar,organization, designation, twitter,lastname,country,city';
        $scenarios['update'][]   = 'role,lastname, avatar,organization, designation, twitter,lastname,country,city';
        $scenarios['register'][] = 'role,lastname, avatar,organization, designation, twitter,lastname,country,city';
        return $scenarios;
    }
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['name','lastname', 'role'], 'required'];
        $rules[] = ['role', 'integer'];
        $rules[] = ['__avatar_file__', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024];
        $rules[] = ['avatar', 'string','max' => 255];
		$rules[] = ['lastname', 'string', 'max' => 255];
        $rules[] = ['organization', 'string','max' => 255];
        $rules[] = ['designation', 'string','max' => 255];
        $rules[] = ['country', 'string','max' => 255];
        $rules[] = ['city', 'string','max' => 255];
         $rules[] = ['hobbies', 'string','max' => 255];
        // add some rules

        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
		$labels['lastname'] = \Yii::t('user', 'Last Name');
        $labels['role'] = \Yii::t('user', 'User Role');
        $labels['avatar'] = \Yii::t('user', 'Profile Photo');
        $labels['organization'] = \Yii::t('user', 'Organization');
        $labels['designation'] = \Yii::t('user', 'Designation');
        $labels['lastname'] = \Yii::t('user', 'Last Name');
        $labels['country'] = \Yii::t('user', 'Country');
        $labels['city'] = \Yii::t('user', 'City');
        $labels['hobbies'] = \Yii::t('user', 'Hobbies');
        return $labels;
    }

    public function getUsercountry()
    {
        return $this->hasOne(Countries::className(), ['ID' => 'country']);
    }

    public function getFullname(){
        return $this->name . " " . $this->lastname;
    }

    public static function roleAlias($code=null)
    {
        $role=array(
                self::USER_USER => 'USER',
                self::USER_CEO => 'CEO',
                self::USER_ADMIN => 'ADMIN',
            );
        if (isset($code)) {
            return $role[$code];
        }
        return $role;
    }

}