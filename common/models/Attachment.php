<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attachment".
 *
 * @property integer $ID
 * @property integer $Message
 * @property string $Attachment
 * @property string $AttachmentName
 * @property string $AttachmentType
 *
 * @property Message $message
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Message', 'Attachment', 'AttachmentName', 'AttachmentType'], 'required'],
            [['Message'], 'integer'],
            [['Attachment', 'AttachmentName'], 'string', 'max' => 500],
            [['AttachmentType'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Message' => 'Message',
            'Attachment' => 'Attachment',
            'AttachmentName' => 'Attachment Name',
            'AttachmentType' => 'Attachment Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['ID' => 'Message']);
    }
}
