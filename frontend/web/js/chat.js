var socket = io.connect('http://kcbchat.farwell-consultants.com:443',{reconnection: false});

socket.emit("new_conn", {
  mydata: {msg: "New connection established!"}
});

socket.on('notification', function (data) {

    var message = JSON.parse(data);

    $( "#notifications" ).prepend( "<p><strong>" + message.name + "</strong>: " + message.message + "</p>" );

});