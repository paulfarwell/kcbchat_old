<?php
/**
 * var $model  chat model
 * 
 */
 //print_r($model);
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container">
    <div class="row">
    	<div class="wizard">
            <div class="wizard-inner">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                        
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                            
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                          
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 4">
                          
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Step 5">
                          
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step6" data-toggle="tab" aria-controls="step6" role="tab" title="Step 6">
                          
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            
                        </a>
                    </li>
                </ul>
            </div>
            <form class="form-horizontal">
             
                  <div class="tab-content">
                     <?php foreach ($model->reviewQuestions as $value):?>
                        <input type="hidden" id="reviewQ" name="Reviews[QuestionsID]" value="<?php echo $value->ID;?>">
                        <input type="hidden" id="reviewUser" name="Reviews[UserID]" value="<?= \Yii::$app->user->identity->id ?>">
                        <input type="hidden" id="reviewChat" name="Reviews[Chat]" value="<?= $model->ID ?>">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="step1">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback1"><?php echo $value->Question_1;?></p>
                            <?php if($value->Answer_set1 == 3):?>
                            <select id="q1"  class="form-control">
                                <option>Select value</option>
                                <option value="0" class="next-step" id="q1">0</option>
                                <option value="1" class="next-step" id="q1">1</option>
                                <option value="2" class="next-step" id="q1">2</option>
                                <option value="3" class="next-step" id="q1">3</option>
                                <option value="4" class="next-step" id="q1">4</option>
                                <option value="5" class="next-step" id="q1">5</option>
                                <option value="6" class="next-step" id="q1">6</option>
                                <option value="7" class="next-step" id="q1">7</option>
                                <option value="8" class="next-step" id="q1">8</option>
                                <option value="9" class="next-step" id="q1">9</option>
                                <option value="10" class="next-step" id="q1">10</option>
                            </select>
                            <?php else:?>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers1->value_1;?>" class="next-step" id="q1"><?php echo $value->reviewanswers1->value_1;?></label>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers1->value_2;?>" class="next-step" id="q1"><?php echo $value->reviewanswers1->value_2;?></label>
                               <?php if($value->reviewanswers1->value_3 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers1->value_3;?>" class="next-step" id="q1"><?php echo $value->reviewanswers1->value_3;?></label><?php endif;?>
                                <?php if($value->reviewanswers1->value_4 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers1->value_4;?>" class="next-step" id="q1"><?php echo $value->reviewanswers1->value_4;?></label><?php endif;?>
                                 <?php if($value->reviewanswers1->value_5 !=NULL):?><label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers1->value_5;?>" class="next-step" id="q1"><?php echo $value->reviewanswers1->value_5;?></label><?php endif;?>
                         <?php endif;?>     
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback2"><?php echo $value->Question_2;?></p>
                             <?php if($value->Answer_set2 == 3):?>
                            <select id="q1"  class="form-control">
                                <option>Select value</option>
                            <option value="0" class="next-step" id="q2">0</option>
                                <option value="1" class="next-step" id="q2">1</option>
                                <option value="2" class="next-step" id="q2">2</option>
                                <option value="3" class="next-step" id="q2">3</option>
                                <option value="4" class="next-step" id="q2">4</option>
                                <option value="5" class="next-step" id="q2">5</option>
                                <option value="6" class="next-step" id="q2">6</option>
                                <option value="7" class="next-step" id="q2">7</option>
                                <option value="8" class="next-step" id="q2">8</option>
                                <option value="9" class="next-step" id="q2">9</option>
                                <option value="10" class="next-step" id="q2">10</option>
                            </select>
                            <?php else:?>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers2->value_1;?>" class="next-step" id="q2"><?php echo $value->reviewanswers2->value_1;?></label>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers2->value_2;?>" class="next-step" id="q2"><?php echo $value->reviewanswers2->value_2;?></label>
                               <?php if($value->reviewanswers2->value_3 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers2->value_3;?>" class="next-step" id="q2"><?php echo $value->reviewanswers2->value_3;?></label><?php endif;?>
                                <?php if($value->reviewanswers2->value_4 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers2->value_4;?>" class="next-step" id="q2"><?php echo $value->reviewanswers2->value_4;?></label><?php endif;?>
                                 <?php if($value->reviewanswers2->value_5 !=NULL):?><label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers2->value_5;?>" class="next-step" id="q2"><?php echo $value->reviewanswers2->value_5;?></label><?php endif;?>
                         <?php endif;?>
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step3">
                        <div class="step3">
                             <?php if($value->Question_3 != NULL):?>
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback3"><?php echo $value->Question_3;?></p>
                             <?php if($value->Answer_set3 == 3):?>
                            <select id="q1"  class="form-control">
                                <option>Select value</option>
                                <option value="0" class="next-step" id="q3">0</option>
                                <option value="1" class="next-step" id="q3">1</option>
                                <option value="2" class="next-step" id="q3">2</option>
                                <option value="3" class="next-step" id="q3">3</option>
                                <option value="4" class="next-step" id="q3">4</option>
                                <option value="5" class="next-step" id="q3">5</option>
                                <option value="6" class="next-step" id="q3">6</option>
                                <option value="7" class="next-step" id="q3">7</option>
                                <option value="8" class="next-step" id="q3">8</option>
                                <option value="9" class="next-step" id="q3">9</option>
                                <option value="10" class="next-step" id="q3">10</option>
                            </select>
                            <?php else:?>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers3->value_1;?>" class="next-step" id="q3"><?php echo $value->reviewanswers3->value_1;?></label>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers3->value_2;?>" class="next-step" id="q3"><?php echo $value->reviewanswers3->value_2;?></label>
                               <?php if($value->reviewanswers3->value_3 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers3->value_3;?>" class="next-step" id="q3"><?php echo $value->reviewanswers3->value_3;?></label><?php endif;?>
                                <?php if($value->reviewanswers3->value_4 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers3->value_4;?>" class="next-step" id="q3"><?php echo $value->reviewanswers3->value_4;?></label><?php endif;?>
                                 <?php if($value->reviewanswers3->value_5 !=NULL):?><label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers3->value_5;?>" class="next-step" id="q3"><?php echo $value->reviewanswers3->value_5;?></label><?php endif;?>
                         <?php endif;?>
                        </div>
                         <?php endif;?>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step4">
                        <div class="step4">
                             <?php if($value->Question_4 != NULL):?>
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback4"><?php echo $value->Question_4;?></p>
                             <?php if($value->Answer_set4 == 3):?>
                            <select id="q1"  class="form-control">
                                <option>Select value</option>
                                <option value="0" class="next-step" id="q4">0</option>
                                <option value="1" class="next-step" id="q4">1</option>
                                <option value="2" class="next-step" id="q4">2</option>
                                <option value="3" class="next-step" id="q4">3</option>
                                <option value="4" class="next-step" id="q4">4</option>
                                <option value="5" class="next-step" id="q4">5</option>
                                <option value="6" class="next-step" id="q4">6</option>
                                <option value="7" class="next-step" id="q4">7</option>
                                <option value="8" class="next-step" id="q4">8</option>
                                <option value="9" class="next-step" id="q4">9</option>
                                <option value="10" class="next-step" id="q4">10</option>
                            </select>
                            <?php else:?>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers4->value_1;?>" class="next-step" id="q4"><?php echo $value->reviewanswers4->value_1;?></label>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers4->value_2;?>" class="next-step" id="q4"><?php echo $value->reviewanswers4->value_2;?></label>
                               <?php if($value->reviewanswers4->value_3 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers4->value_3;?>" class="next-step" id="q4"><?php echo $value->reviewanswers4->value_3;?></label><?php endif;?>
                                <?php if($value->reviewanswers4->value_4 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers4->value_4;?>" class="next-step" id="q4"><?php echo $value->reviewanswers4->value_4;?></label><?php endif;?>
                                 <?php if($value->reviewanswers4->value_5 !=NULL):?><label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers4->value_5;?>" class="next-step" id="q4"><?php echo $value->reviewanswers4->value_5;?></label><?php endif;?>
                         <?php endif;?>
                        </div>
                        <?php endif;?>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step5">
                        <div class="step5">
                            <?php if($value->Question_5 != NULL):?>
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback5"><?php echo $value->Question_5;?></p>
                            <?php if($value->Answer_set5 == 3):?>
                            <select id="q1"  class="form-control">
                                <option>Select value</option>
                                <option value="0" class="next-step" id="q5">0</option>
                                <option value="1" class="next-step" id="q5">1</option>
                                <option value="2" class="next-step" id="q5">2</option>
                                <option value="3" class="next-step" id="q5">3</option>
                                <option value="4" class="next-step" id="q5">4</option>
                                <option value="5" class="next-step" id="q5">5</option>
                                <option value="6" class="next-step" id="q5">6</option>
                                <option value="7" class="next-step" id="q5">7</option>
                                <option value="8" class="next-step" id="q5">8</option>
                                <option value="9" class="next-step" id="q5">9</option>
                                <option value="10" class="next-step" id="q5">10</option>
                            </select>
                            <?php else:?>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers5->value_1;?>" class="next-step" id="q5"><?php echo $value->reviewanswers5->value_1;?></label>
                                <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers5->value_2;?>" class="next-step" id="q5"><?php echo $value->reviewanswers5->value_2;?></label>
                               <?php if($value->reviewanswers5->value_3 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers5->value_3;?>" class="next-step" id="q5"><?php echo $value->reviewanswers5->value_3;?></label><?php endif;?>
                                <?php if($value->reviewanswers5->value_4 !=NULL):?> <label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers5->value_4;?>" class="next-step" id="q5"><?php echo $value->reviewanswers5->value_4;?></label><?php endif;?>
                                 <?php if($value->reviewanswers5->value_5 !=NULL):?><label class="radio-inline"><input type="radio" value="<?php echo $value->reviewanswers5->value_5;?>" class="next-step" id="q5"><?php echo $value->reviewanswers5->value_5;?></label><?php endif;?>
                         <?php endif;?>
                        </div>
                    <?php endif;?>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="step6">
                        <div class="step6">
                     <div class="form-group">
                            <p style="color:#064367;font-weight: bold;" id="feedback6">Please Leave a comment </p>
                             <textarea id="comment" class="form-control" rows="4" cols="50" style="height:auto"></textarea><br><br>
                            <input type="submit" class="btn btn-custom pull-right next-step" value="Send Feedback" id="q6">
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane " role="tabpanel" id="complete">
                        <div class="complete">
                        <h2>Thank You for Giving Your Feedback on the Chat.</h2>
                        <br>
                        <a href="<?= Url::toRoute(['chat/index',])?>" class="btn btn-custom">Proceed To Home Page</a>
                    </div>
                    </div>
                <?php endforeach;?>
                </div>
                </form>

                
        </div>
   </div>
</div>
