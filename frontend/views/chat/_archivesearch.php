<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Chat;
use backend\models\ChatSearch;
/* @var $this yii\web\View */
/* @var $model backend\models\ChatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-search">
    <?php $form = ActiveForm::begin([
        'action' => ['archives'],
        'method' => 'get',
        'layout' => 'inline',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => '',
                'wrapper' => 'col-sm-4',
                'error' => '',
                'hint' => '',
            ],
        ]

    ]); ?>
 
    
    <?= $form->field($model, 'Year')->dropDownList(ArrayHelper::map(ChatSearch::getYears(),'year','year'),['prompt'=>'Select Year',]) ?>

    <?= $form->field($model, 'Quarter')->dropDownList(ChatSearch::getQuarters(),['prompt'=>'Quarter']) ?>

    <?= $form->field($model, 'Topic')->dropDownList(ArrayHelper::map(ChatSearch::getTopics(),'topic','topic'),['prompt'=>'Topic']) ?>

    <?= $form->field($model, 'Host')->dropDownList(ArrayHelper::map(Chat::find()->where(['Status' => Chat::CHAT_COMPLETED])->orderBy('ID')->all(),'Host','ceostring'),['prompt'=>'Host']) ?>


   

    
        <button type="submit" class="btn btn-danger"><i class="fa fa-search fa-1x"></i></button>
    

    <?php ActiveForm::end(); ?>
</div>