<?php
// _chats_view.php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Chat;
use Zelenin\yii\SemanticUI\Elements;
?>
<div class="col-xs-12 col-md-6 spec_chat_details">
	
	<div class="row this-chat chat_details  border-right">
		<div class="media">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if(empty($model->host->profile->avatar)):?>
					<div class="home_image_holder">
                   <?= Html::img('@web/images/avatar/user.jpg',['class' => ' img-responsive'])?>
                   </div>
			    <?php else:?>
				<div class="home_image_holder">
					<!-- @web/images/avatar/'.$model->host->profile->avatar -->
				<?= Html::img('@web/images/avatar/'.$model->host->profile->avatar,['class' => ' archive_image  img-responsive'])?>
			      </div>
		<?php endif;?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="media-body">
					<div class="row" id="date">
						<div class="col-lg-2 col-xs-3 medium-bold">
							<?= Html::encode("DATE: "); ?>
						</div>
						<div class="col-lg-10 col-xs-9">
							<?= Html::encode($model->ChatDate); ?>
						</div>
					</div>
					<div class="row" id="time">
						<div class="col-lg-2 col-xs-3 medium-bold">
							<?= Html::encode("TIME: "); ?>
						</div>
						<div class="col-lg-10 col-xs-9">
							<?= Html::encode(Yii::$app->formatter->asTime($model->StartTime, "HH:mm a")." - ".Yii::$app->formatter->asTime($model->EndTime, "HH:mm a")); ?>
						</div>
					</div>
					<div class="row" id="host">
						<div class="col-lg-2 col-xs-3 medium-bold">
							<?= Html::encode("HOST: "); ?>
						</div>
						<div class="col-lg-10 col-xs-9">
							<?php if(empty($model->ceostring)):?>
                               <?= Html::encode($model->hoststring); ?>
							<?php else:?>
							<?= Html::encode($model->ceostring); ?>
						<?php endif;?>
						</div>
					</div>
					<div class="row" id="topic">
						<div class="col-lg-2 col-xs-3 medium-bold">
							<?= Html::encode("TOPIC: "); ?>
						</div>
						<div class="col-lg-10 col-xs-9">
							<?= Html::encode($model->Topic); ?>
						</div>
					</div>
				</div> <!-- media-body -->
                </div>

		</div> <!-- media -->
	</div> <!-- row this-chat chat_details -->
 <div class="row actions">
 	
		<div class="media-body">
          <ul class="chat-view-ul" style="margin-left:-26px;">
          	<li>
			<?php if ($model->Log != NULL):?>
				<?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]).'Watch .', Url::toRoute(['chat/view', 'id' => $model->ID]), ['title' => $model->Topic . " Read"]) ?>
			<?php else:?>
				<?php echo Html::tag('span', '', [ 'aria-hidden' => 'true',]). "Watch ."; ?>
			<?php endif;?>

          	</li>
          	<li>
          		<?php if ($model->Podcast != NULL):?>
				<?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]). 'Listen .', Url::toRoute(['chat/view', 'id' => $model->ID]), ['title' => $model->Topic . "Watch"]) ?>
			<?php else:?>
				<?php echo Html::tag('span', '', [ 'aria-hidden' => 'true',]). " Listen ."; ?>
			<?php endif;?>
          	</li>
          	<li>
          		<?php if ($model->Log != NULL):?>
				<?= Html::a(Html::tag('span', '', [ 'aria-hidden' => 'true',]).'Read ', Url::toRoute(['chat/view', 'id' => $model->ID]), ['title' => $model->Topic . " Read"]) ?>
			<?php else:?>
				<?php echo Html::tag('span', '', [ 'aria-hidden' => 'true',]). " Read"; ?>
			<?php endif;?>
			</span>
          	</li>
          	
          </ul>
          </div>
          <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
			<div id="details">
			 <?= Html::a('Details', Url::toRoute(['chat/details', 'id' => $model->ID]), ['title' => $model->Topic . "Details",  'class' => "btn btn-join "]) ?>
			</div>
		</div>
		 <!-- col-xs-3 -->
        <!-- col-xs-3 -->
	</div><!-- row actions--> 
</div><!-- MAIN col-xs-12 col-md-6 -->
