<?php
/* @var $this yii\web\View */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
?>
<?php
$this->title = Yii::t('user', 'KCB Chat');
$this->registerCssFile('/css/slick.css');
$this->registerCssFile('/css/slick-theme.css');
$this->registerJsFile('/js/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/chathelper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);



$js = <<<JS
 $('#events_section').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 8000,
  nextArrow: '<i class="arrow-right slick-next"></i>',
  prevArrow: '<i class="arrow-left slick-prev"></i>',
  responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true
        }
      },
    ]
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
$this->registerJs("
  var openigtime = moment(".strtotime($currentchat->OpenTime).");
  var chatStatus = ".$currentchat->Status.";
  var chatRoom = ".$currentchat->ID.";
  var mymemented=$('.memented');
  ", \yii\web\View::POS_END, 'current-chat');

AppAsset::register($this);
$detector = new \Mobile_Detect();
$bodyclass = "";

if ($detector->isMobile()) {
    $bodyclass = "mobile";
}

?>


<?php
$js2 = <<<JS
 $('#chat_summary_slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplay: true,
  autoplaySpeed: 8000,
  nextArrow: '<i class="fa fa-arrow-right slick-next"></i>',
  prevArrow: '<i class="fa fa-arrow-left slick-prev"></i>',
  dots: false,
  arrows:true,
  responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true,
        }
      },
    ]
});
JS;
$this->registerJs($js2, \yii\web\View::POS_READY);
?>
<?php 
$jsspec = <<<JS
$('.spec_chat_details .media').matchHeight();
JS;
$this->registerJs($jsspec, \yii\web\View::POS_READY);
?>
<?= $this->render('@frontend/views/_confirmalert'); ?>
<?php if($bodyclass === "mobile"):?>
<?= $this->render('@frontend/views/templates/mobile-home-jumbo',['chats'=>$chats]); ?>
<?php else:?>

<?= $this->render('@frontend/views/templates/home-jumbo',['chats'=>$chats, 'banners'=>$banners]); ?>


<?php endif;?>

<section class="chats_slider_title">
    <label class="text-center"> <?= Html::img('@web/images/icons/chat-view.png',['class' => 'img-responsive','style'=>'width:60px; height:60px; display:inline-flex;'])?>Chat Forums <?php echo date("Y");?> Edition <hr></label>
    
</section>
  
<section class="chat-details-container container-orange">

    <div class="container"> <!-- main container -->

        <div class="row"> <!-- main-row -->
         <div class="row">
           <i class="fa fa-chevron-right pull-right fa-1x" style="color:#95C13D; margin-right:12%"></i>
        <i class="fa fa-chevron-left pull-right fa-1x" style="color:#95C13D"></i>
         </div>
        
        <?= 
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                // 'tag' => 'div',
                'class' => 'chat_summary_slider',
                'id' => 'chat_summary_slider',
            ],
            'layout' => "{items}\n{pager}",
            'itemView' => '_chats_view',
            'itemOptions' => [
                'tag' => false,
            ],
            'pager' => [
                'prevPageLabel' => '<i class="fa fa-arrow-left slick-prev slick-arrow"></i>',
                'nextPageLabel' => '<i class="fa fa-arrow-right slick-prev slick-arrow"></i>',
                'options' => [
                    'class' => 'pagination col-xs-12 col-lg-12 col-md-12 col-sm-12'
                ],
                'maxButtonCount' => 1,
            ],
        ]); 
        ?>

        </div> <!-- main-row -->

        <div class="row archives_index_btn">
          <div id="chat_archive_section">
            <div class="col-md-6 col-md-offset-6">
                <span class="pull-right">
                <?php echo Html::a(Html::img('@web/images/icons/archives.png', ['class' => 'img-responsive chat_archive_img']), ['/chat/archives']); ?> 
                </span>
            </div>
          </div>
        </div><!-- Chat Archive -->
        
    </div> <!-- main container -->
</section> <!-- container-section -->