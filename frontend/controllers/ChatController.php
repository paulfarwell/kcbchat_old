<?php

namespace frontend\controllers;

use Yii;
use yii\web\IdentityInterface;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use backend\models\Profile;
// use yii\data\Pagination;
use common\models\Reviews;
use common\models\Chat;
use common\models\Edition;
use common\models\Event;
use common\models\Message;
use common\models\Online;
use common\models\Participate;
use backend\models\ChatSearch;
use kartik\mpdf\Pdf;


class ChatController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'details', 'join', 'enter'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'details', 'download'],

                    ],
                    [
                        'allow' => true,
                        'actions' => ['join', 'enter'],
                        'roles' => ['@'],
                    ],
                ],
                'user' => [
                      'loginUrl' => [Url::toRoute(['site/login'])],
                ],
            ],
        ];
    }

    // public function actionHosts($id)
    // {
    //     $model = $id;
    //     return $this->render('host',[
    //         'model' => $model,
    //     ]);
    // }

    public function actionDetails($id)
    {
        $model = $this->findModel($id);
        return $this->render('details',[
            'model' => $model,
        ]);
    }


    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->Status != Chat::CHAT_COMPLETED) {
           return $this->redirect(['details', 'id' => $model->ID]);
        }else{
            $messages = Message::find()->where(['Chat' => $id])->all();
            return $this->render('view', [
                'model' => $model,
                'messages' => $messages,
            ]);
        }
        
    }

    public function actionIndex()
    {   

        $currentedition = Edition::find()->where(['Current' => Edition::EDITION_CURRENT])->orderBy('ID')->one();
        $query = Chat::find()->where(['Edition' => $currentedition->ID]);

        $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                    'pageSize' => 10,
                ],
             'sort'=> ['defaultOrder' => ['ID'=>SORT_ASC]]
        ]);
        $eventquery = Event::find()->where(['Edition' => $currentedition->ID]);

        $eventsProvider = new ActiveDataProvider([
             'query' => $eventquery,
             'pagination' => [
                    'pageSize' => 10,
                ],
             'sort'=> ['defaultOrder' => ['ID'=>SORT_ASC]]
        ]);

        $events = $eventsProvider->getModels();
        $currentchat = Chat::getnextchat();
        $chats = Chat::getUpcomingChats();
         $datetoday= date("Y-m-d");
        // var_dump('ChatDate');die;
        $banners = Chat::find()->where('ChatDate > :date', [':date' => $datetoday])->orderBy(['ChatDate'=> SORT_ASC])->one();
        //print_r($events);
        //$chats = $dataProvider->getModels();

        return $this->render('index', [
            'currentedition' => $currentedition,
            'dataProvider' => $dataProvider,
            'events' => $events,
            'currentchat'=> $currentchat,
            'chats'=>$chats,
            'banners'=>$banners,
        ]);
    }

    /**
    *register a user to the chat
    *prevent users from joining non active chats
    */
    public function actionJoin($id)
    {


        $model = $this->findModel($id);
        $user = Yii::$app->user->identity->profile->user_id;
        $role = Profile::roleAlias(Yii::$app->user->identity->profile->role);
        $host = $model->host->profile->user_id;
        if($host === $user){
         $viewfile = "join_host";

        }else if ($role=="USER") {

          $viewfile ="join_user";

        }else{
          $viewfile ="join";
        }
        /**
         * Create a request to participate if none exists
         *
         */
        $this->checkParticipateStatus($user,$id);
        

        if ($model->Status != Chat::CHAT_ACTIVE) {
           return $this->redirect(['index', 'id' => $model->ID]);
        }
        /*
        * Ii user manages to join make the user online
        */
        $this->makeOnline($user,$id);
        if (Yii::$app->request->post()) {
            
            $message = Yii::$app->request->post('message');
            $siteUrl=Url::home();
            $messageModel = new Message();
            $messageModel->Chat = $id;
            $messageModel->User = Yii::$app->user->identity->id;
            $messageModel->UserName= Yii::$app->user->identity->profile->Fullname;
            $messageModel->Status = 1;
            $messageModel->MessageType = 1;
            $messageModel->MessageText = $message;
            $messageModel->replyID = 0;
            $messageModel->role = 1;
            $messageModel->mentionID = 1;

            $messageModel->save();

            return Yii::$app->redis->executeCommand('PUBLISH', [
                'channel' => 'notification',
                'message' => Json::encode([ 
                    'id' => $messageModel->ID, 
                    'message' => $messageModel->MessageText, 
                    'chatKey' => Yii::$app->user->identity->auth_key, 
                    'maxUsers' => $model->ParticipantsLimt, 
                    'chatRoom' => $messageModel->Chat, 
                    'chatUserName' => $messageModel->UserName,
                    'chatUserId' => $messageModel->User,
                    'role'          => Profile::roleAlias(Yii::$app->user->identity->profile->role),
                    'avatar'        => Yii::$app->user->identity->profile->avatar,

                ])
            ]);

        }
         
        $chatvariables = array(
            'chatKey'       =>  Yii::$app->user->identity->auth_key,
            'id'            =>  $id,
            'maxUsers'      =>  $model->ParticipantsLimt,
            'chatRoom'      =>  $id,
            'chatUserName'  => Yii::$app->user->identity->profile->Fullname,
            'chatUserId'    => Yii::$app->user->identity->id,
            'siteUrl'       => Url::home(),
            'role'          => Profile::roleAlias(Yii::$app->user->identity->profile->role),
            'avatar'        => Yii::$app->user->identity->profile->avatar,
            'avatarpath'    => Yii::$app->user->identity->profile->getImgSrc('avatar'),
            'host'          => $model->host->profile->user_id,
            'hostname'      => $model->Ceostring,
            //'role'          => "'.strtolower(Yii::app()->user->role).'";
        );
        //Yii::$app->session['room'] = $id;
        $this->layout = 'chat_body';
        Yii::$app->session->setFlash('success', Yii::t('user', 'Welcome back to the chat.'));
        return $this->render($viewfile, [
            'model' => $model,
            'chatvariables' => $chatvariables,
        ]);
    }


    public function actionArchives(){
        $searchModel = new ChatSearch();
        $dataProvider = $searchModel->searcharchives(Yii::$app->request->queryParams);
        return $this->render('archives', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDownload($id){
        $model = $this->findModel($id);
        $path = Yii::getAlias('@webroot') . '/chatlogs/';
        $file = $path . $model->Log;
        if (file_exists($file)) {
            return \Yii::$app->response->sendFile($file);
        }
    }


    /**
     * Finds the Chat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function checkParticipateStatus($user,$id){

        /*
        *Update to get participate consants from participate models
        */
        $chat = $this->findModel($id);
        $online = Online::find()->where(['User' =>$user,'CurrentChat' =>$id])->one();
        $participatedetails = Participate::find()->where(['User' =>$user,'Chat' =>$id])->one();
        if (!$online) {
           if (!$participatedetails) {
                $participatemodel =  new Participate();
                $participatemodel->User = $user;
                $participatemodel->Chat = $id;
                $participatemodel->Termsagree = 1;
                $participantsCount = Participate::find()->where(['Chat' =>$id])->count();
                if ($participantsCount < $chat->ParticipantsLimt) {
                   $participatemodel->Status = 1;
                }else{
                    $participatemodel->Status = 2;
                }
                $participatemodel->save();
            }else{
                $participantsCount = Participate::find()->where(['Chat' =>$id])->count();

                if ($participantsCount < $chat->ParticipantsLimt) {
                   $participatedetails->Status = 1;
                   $participatedetails->save();
                }
                //$this->makeOnline($user,$id);
            }
        }elseif ($online && (!$participatedetails)) {
                $participatemodel =  new Participate();
                $participatemodel->User = $user;
                $participatemodel->Chat = $id;
                $participatemodel->Termsagree = 1;
                $participantsCount = Participate::find()->where(['Chat' =>$id])->count();
                if ($participantsCount < $chat->ParticipantsLimt) {
                   $participatemodel->Status = 1;
                }else{
                    $participatemodel->Status = 2;
                }
                $participatemodel->save();
        }else{
            Yii::$app->session->setFlash('success', Yii::t('user', 'Welcome back to the chat.'));
        }
        
        // print_r($participatemodel);
        // die;
    }
    public function makeOnline($user,$id){
        $online = Online::find()->where(['User' =>$user,'CurrentChat' =>$id])->one();
        if (!$online) {
            $onlinemodel =  new Online();
            $onlinemodel->User = $user;
            $onlinemodel->CurrentChat = $id;
            $onlinemodel->UserName = Yii::$app->user->identity->profile->name;
            $onlinemodel->Status = 0;
            $onlinemodel->Ip =  ip2long(Yii::$app->request->userIP);
            $onlinemodel->save();
            Yii::$app->session->setFlash('success', Yii::t('user', 'Welcome to the chat.'));
        }else{
            Yii::$app->session->setFlash('success', Yii::t('user', 'Welcome back to the chat.'));
        }
    }
	
	public function actionEnter(){
        $nextchat = Chat::getnextchat();
        $id =  $nextchat->ID;
        $user = Yii::$app->user->identity->id;
        $participatedetails = Participate::find()->where(['User' =>$user,'Chat' =>$id])->one();
        if(!$participatedetails){
            $participatemodel =  new Participate();
            $participatemodel->User = $user;
            $participatemodel->Chat = $id;
            $participatemodel->Termsagree = 1;
            $participantsCount = Participate::find()->where(['Chat' =>$id])->count();

            if ($participantsCount < $nextchat->ParticipantsLimt) {
               $participatemodel->Status = 1;
            }else{
                $participatemodel->Status = 2;
            }
        $participatemodel->save();
        }
        if ($nextchat->Status == Chat::CHAT_ACTIVE) {
            return $this->redirect(['chat/join', 'id' => $id]);
        }else{
            return $this->redirect(['chat/view', 'id' => $id]);
            Yii::$app->session->setFlash(
                'info',
                Yii::t('user', 'There is no active chat at the moment!')
            );
        }
    }


    // public function actionTranscript($id){


    // }


     public function actionStorereview(){

        //var_dump(Yii::$app->request->post());die;
       
       $question = $_POST['question'];
       $chat = (int)$_POST['Chat'];
       $QuestionsID = (int)$_POST['QuestionsID'];
       $UserID = (int)$_POST['UserID'];
       $answer = $_POST['review'];
       $comment = $_POST['comment'];
        
        $user = Review::find()->where(['UserID'=>$UserID, 'Chat'=>$chat]);



        // var_dump($chat, $QuestionsID, $UserID, $answer,$question);
        if($question === 'q1' && empty($user)){
            $model = new Reviews();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_1 = $answer;
            if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }elseif($question === 'q1' && !empty($user)){

            $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$user])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_1 = $answer;
             if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }
         else if($question === "q2"){
           
           $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$UserID])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_2 = $answer;
             if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }else if($question === "q3"){
           
           $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$UserID])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_3 = $answer;
             if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }else if($question === "q4"){
           
           $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$UserID])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_4 = $answer;
             if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }else if($question === "q5"){
           
           $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$UserID])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->Answer_5 = $answer;
             if($model->save()){
                 echo json_encode(['data'=>'OK']);
            }else{
                echo json_encode(['data'=>'err']);
            }
         }else{
           
           $model = Reviews::find()->where(['Chat' =>$chat,'QuestionsID' =>$QuestionsID,'UserID'=>$UserID])->one();
            $model->Chat = $chat;
            $model->QuestionsID = $QuestionsID;
            $model->UserID = $UserID;
            $model->comment = $comment;
            if($model->save()){

                echo json_encode(['data'=>'OK']);
            }
            
         }
       
     }


      public function actionChatpdf($id) {
        $model = Chat::find()->where(['ID'=>$id])->one();
        $messages = Message::find()->where(['Chat' => $id])->all();
         $content =  $this->renderPartial('transcript_pdf',['model'=>$model,'messages'=>$messages]);

        //var_dump($model);die();
    $pdf = new Pdf([
        'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
        'content' =>$content,
        'options' => [
            'title' => 'Chat Transcript',
            'subject' => 'Generating Chat Transcript'
        ],
        'methods' => [
            'SetHeader' => ['Generated By: KCB Group ||Generated On: ' . date("r")],
            'SetFooter' => ['|Page {PAGENO}|'],
        ]
    ]);
    return $pdf->render();
}

   
}