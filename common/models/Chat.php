<?php

namespace common\models;

use Yii;
use frontend\models\User;
use backend\models\ReviewQuestions;
use backend\models\Host;

/**
 * This is the model class for table "chat".
 *
 * @property integer $ID
 * @property integer $Edition
 * @property string $ChatDate
 * @property string $StartTime
 * @property string $EndTime
 * @property string $Topic
 * @property integer $Host
 * @property string $Description
 * @property integer $Status
 * @property string $Log
 * @property string $Podcast
 * @property integer $ParticipantsLimt
 *
 * @property User $host
 * @property Edition $edition
 * @property Message[] $messages
 */
class Chat extends \yii\db\ActiveRecord
{
    //the following constants are used to define the status of the chat . Pending, Active Completed
    const CHAT_PENDING = 0;
    const CHAT_ACTIVE = 1;
    const CHAT_COMPLETED = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Edition', 'ChatDate', 'StartTime', 'EndTime', 'Topic', 'ParticipantsLimt', 'Status'], 'required'],
            [['Edition', 'chatHost', 'Status', 'ParticipantsLimt'], 'integer'],
            [['ChatDate'], 'safe'],
            [['Description'], 'string'],
            [['StartTime', 'EndTime'], 'string', 'max' => 12],
            [['Topic', 'Log', 'Podcast','chat_banner'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Edition' => 'Edition',
            'ChatDate' => 'Chat Date',
            'StartTime' => 'Start Time',
            'EndTime' => 'End Time',
            'Topic' => 'Topic',
            'chatHost' => 'chatHost',
            'Description' => 'Description',
            'Status' => 'Status',
            'Log' => 'Log',
            'Podcast' => 'Podcast',
            'ParticipantsLimt' => 'Participants Limt',
             'chat_banner'=> 'Chat Banner'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHost()
    {
        return $this->hasOne(Host::className(), ['id' => 'chatHost']);
    }

  public function getHosts()
    {
        return $this->hasOne(User::className(), ['id' => 'chatHost']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdition()
    {
        return $this->hasOne(Edition::className(), ['ID' => 'Edition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['Chat' => 'ID']);
    }

     public function getReviewQuestions()
    {
        return $this->hasMany(ReviewQuestions::className(), ['Chat' => 'ID']);
    }
    public function getReviewAnswers(){
        return $this->hasMany(Reviews::className(), ['Chat'=>'ID']);
    }
    public function getCurrent(){

        return $this->hasMany(Online::className(), ['CurrentChat' =>'ID']);
    }

    public function getOpenTime(){
        return $this->ChatDate." ".$this->StartTime;
    }
    
   
    public function getCeoString()
    {
        if($this->host!==null){
            if ($this->host->profile!==null) {
                if (isset($this->host->profile->name)) {
                    return $this->host->profile->name. " " . $this->host->profile->lastname;
                }
            }
        }
    }

    public function getHostString(){

        if($this->host!==null){
           
            return $this->host->first_name. " " . $this->host->last_name;
               
        }
    }
    public static function chatStatus($code=null)
    {
        $current=array(
                self::CHAT_PENDING => 'PENDING',
                self::CHAT_ACTIVE => 'ACTIVE',
                self::CHAT_COMPLETED => 'COMPLETED',
            );
        if (isset($code)) {
            return $current[$code];
        }
        return $current;
    }
	
	public static function getnextchat(){
        $datetoday= date("Y-m-d", time() + 86400);
        $condition='ChatDate>"'.$datetoday.'"';
        $sql='SELECT * FROM chat WHERE ChatDate >=(CURDATE()) ORDER BY ChatDate ASC LIMIT 1';
        $chat = self::findBySql($sql)->one();
        if ($chat) {
            return $chat;
        }else{
            $sql='SELECT * FROM chat ORDER BY ChatDate DESC LIMIT 1';
            $chat = self::findBySql($sql)->one();
            return $chat;
        }
    }
     
    public function getupcomingchat(){
        $chatdate=strtotime($this->ChatDate);
        $datetoday= date("Y-m-d");
        $condition='ChatDate>"'.$datetoday.'"';
        $sql='SELECT * FROM chat WHERE ChatDate >"'.$datetoday .'" ORDER BY ChatDate ASC LIMIT 1';
        $chat = self::findBySql($sql)->one();
        if ($chat) {
            return $chat;
        }else{
            $sql='SELECT * FROM chat ORDER BY ChatDate DESC LIMIT 1';
            $chat = self::findBySql($sql)->one();
            return $chat;
        }
    }

    public static function getUpcomingChats(){

         $date = date("Y-m-d");
         $chats = self::find()->where('ChatDate > :date', [':date' => $date])->all();
         return $chats;
    }

    

  
}
