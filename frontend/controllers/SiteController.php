<?php
namespace frontend\controllers;

use Yii;
use common\models\Attachment;
use backend\models\Profile;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\RegistrationForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use dektrium\user\traits\AjaxValidationTrait;
use dektrium\user\models\LoginForm;
use dektrium\user\Module;
use backend\models\ReviewQuestions;
use common\models\Reviews;
use common\models\User;
use kartik\mpdf\Pdf;


/**
 * Site controller
 */
class SiteController extends Controller
{
    use AjaxValidationTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post()) {

            $name = Yii::$app->request->post('name');
            $message = Yii::$app->request->post('message');

            return Yii::$app->redis->executeCommand('PUBLISH', [
                'channel' => 'notification',
                'message' => Json::encode(['name' => $name, 'message' => $message])
            ]);

        }
        return $this->render('index');
    }

    /**
     * Register  a user.
     *
     * @return mixed
     */

    public function actionRegister()
    {
        $usermodule = $this->module->modules['user'];
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $usermodule =  $this->module->modules['user'];


        $loginmodel = Yii::createObject(LoginForm::className());
        $this->performAjaxValidation($loginmodel);
        
        // if (!$this->module->enableRegistration) {
        //     throw new NotFoundHttpException();
        // }

        if ($loginmodel->load(Yii::$app->getRequest()->post()) && $loginmodel->login()) {
            return $this->goBack();
        }

        /** @var RegistrationForm $model */
        $model = Yii::createObject(RegistrationForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return $this->render('/message', [
                'title'  => Yii::t('user', 'Your account has been created'),
                'module' => $usermodule,
            ]);
        }

        return $this->render('register', [
            'model'  => $model,
            'loginmodel'  => $loginmodel,
            'module' => $usermodule,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    /*
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }*/
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $usermodule = $this->module->modules['user'];
        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $user = User::findOne(Yii::$app->user->identity->id);
            if($user->email == null){
               $name = Yii::$app->user->identity->username;
              $ldapObject = \Yii::$app->ad->search()->findBy('sAMAccountName',$name);
              $user->email = $ldapObject['userprincipalname'][0];
              $user->save();
              
             $profile = new Profile();
             $profile->user_id = Yii::$app->user->identity->id;
             $profile->name = $ldapObject['givenname'][0];
             $profile->lastname = $ldapObject['sn'][0];
             $profile->public_email = $ldapObject['mail'][0];
             $profile->employee_id = $ldapObject['employeeid'][0];
             $profile->manager = $ldapObject['manager'][0];
             $profile->title = $ldapObject['title'][0];
             $profile->branch = $ldapObject['physicaldeliveryofficename'][0];
             $profile->role = 1;
             $profile->organization= $ldapObject['department'][0];
             $profile->designation= $ldapObject['department'][0];
             $profile->country = $ldapObject['company'][0];

             $profile->save();

               return $this->goBack();
            }else{

              return $this->goBack();
            }
           
           //  var_dump($user->save(),$user->getErrors());die;
           // return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'module' => $usermodule,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionHelp(){

        return $this->render('help');
    }

   public function actionAutocomplete(){
        $term = $_POST['term'];
        $data = Profile::find()->where(['like','name',$term])->all();
        //var_dump($data);die;
          $result=array();
         foreach ($data as $key => $v){
              $result[]=['user_id'=>$v->user_id,'firstname' =>$v->name,'lastname' =>$v->lastname];
           }
         echo json_encode(['data'=>$result]);


    }

    public function actionFileupload(){
        $file = $_FILES['file-input']['name'];
        $fileType = $_FILES['file-input']['type'];
         $target_dir = $_SERVER["DOCUMENT_ROOT"]."uploads/";
          
          if ($fileType=="application/pdf" || $fileType=="image/gif" || $fileType=="image/jpeg" || $fileType == "image/png") {

          if (move_uploaded_file($_FILES["file-input"]["tmp_name"], $target_dir . $_FILES["file-input"]["name"])){

             $upload[] = ['file'=>$file, 'filetype'=>$fileType];

             echo json_encode(['data'=>$upload]);
          }else{
              $response['status'] = 'err';
         }
     }

    }

     public function actionAttachment($id){
       $id = (int)$id;
      $model = Attachment::find()->where(['=','ID',$id])->one();
      $path = Yii::getAlias('@webroot') . '/uploads/';
      $file = $path . $model->AttachmentName;
      if(file_exists($file)){

        return \Yii::$app->response->sendFile($file);
      }
       
    }

    public function actionReview($id){

      $id = (int)$id;
      $questions = ReviewQuestions::find()->where(['=','Chat',$id])->one();
      $model = new Reviews();
      
     return $this->render('review', [
            'model' => $model,
            'question'=>$questions
        ]);
    }


    public function actionStorereview(){

      // var_dump(Yii::$app->request->post());die;

      $model = new Reviews();
       // new record
        if($model->load(Yii::$app->request->post()) && $model->save()){
          Yii::$app->session->setFlash('success', 'Thank You for Giving Your Feedback on the Chat.');
            return $this->goHome();
        }
                 
       
    }

   
}
