<?php
namespace frontend\models;

use dektrium\user\models\SettingsForm as BaseSettingsForm;
/**
 * account update form
 */
class SettingsForm extends BaseSettingsForm
{
    public $new_password_repeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        // unset($rules['currentPasswordRequired']);
        $rules[] = ['new_password_repeat', 'string', 'min' => 6];
        $rules[] = ['new_password_repeat', 'required'];
        $rules[] = ['new_password_repeat','compare','compareAttribute'=>'new_password'];
        return $rules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['new_password_repeat'] = \Yii::t('user', 'Repeat New Password');
        return $labels;
    }


}
