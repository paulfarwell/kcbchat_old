<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
$detector = new \Mobile_Detect();
$bodyclass = "";

if ($detector->isMobile()) {
    $bodyclass = "mobile";
}

?>

<?php
// $noconflict = <<<JS
//   $.fn.bsModal = $.fn.modal.noConflict();
// JS;

$jshideflash = <<<JS
setTimeout(function() {
    $('.alert').fadeOut('slow');
}, 4500); // <-- time in milliseconds
JS;

// $jsfootermodal = <<<JS
// $("#terms_conditions").on("click", function(event) {
//     event.preventDefault();
//     $("#theterms").modal("show");
// });
// JS;

// $this->registerJs($noconflict, \yii\web\View::POS_READY);
$this->registerJs($jshideflash, \yii\web\View::POS_READY);
// $this->registerJs($jsfootermodal, \yii\web\View::POS_READY);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/kcb.png" type="image/x-icon" />
    <?= $this->registerJsFile('/js/socket.io.js', ['position' => \yii\web\View::POS_HEAD]); ?>
    <?= $this->registerJsFile('/js/to-top.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
    <?php $this->head() ?>
</head>
<body id="page-top" class="index <?php echo $bodyclass;?>  ">
<div class="wrapper">
<?= $this->render('@frontend/views/templates/_chat_banner') ?>

<?php $this->beginBody() ?>

<?= $content ?>
<?= $this->render('@frontend/views/templates/_terms-conditions') ?>
</div><!-- wrapper -->
<div class="push"></div>



<footer class="footer-section">
    <div class="container">

        <div class="row">

            <div class="col-md-6 col-xs-12">
                <span class="copyright">
                    Copyright &copy; KCB Group Limited <p class="year"><?= date('Y') ?></p> &nbsp;&nbsp; | &nbsp;&nbsp; <!-- <a href="#" id="terms_conditions" data-toggle="modal" data-target="#theterms">Terms of Use</a> -->
                </span>
            </div>
            
            <div class="col-md-6 col-xs-12">
                <a href="
                <?= Url::to('http://www.farwell-consultants.com');?>
                " class ='farwell_link' target = '_blank'> Developed by FCL</a>

               
            </div>

        </div><!-- main footer row -->

    
        <span class="back-to-top" style="display: inline;"></span>
    </div>
</footer>

<?php $this->endBody() ?>
 <script>
     $(document).ready(function(){
     $("#message-field").on("keyup",function(){
        var str = $(this).val();
        if(str.indexOf("@") == 0 && str.length > 1){
          var term = str.slice(1,30);
          console.log(term)
          $.ajax({
        type: "POST",
        url: "/site/autocomplete",
        data:'term='+ term,
        success: function(strData){
             var data = JSON.parse(strData);
             var options
            data.data.forEach(function(user){
             options += '<option value="' + user.user_id + '">' + user.firstname +' '+user.lastname+ '</option>';
             
             });
            $('select#mentions').show();
             $("select#mentions").html(options);
             // $("#suggesstion-box").show();
             // $("#suggesstion-box").html(data);
            // $("#message-field").css("background","#FFF");
        }
        });
        }else{

            console.log("not")
        }     
    });

$("#mentions").on("change",function(){
    var m = document.getElementById('mentions');
     document.getElementById('message-field').value = "";
     var f = m.options[m.selectedIndex].text; 
     var mention = m.options[m.selectedIndex].value;
    $('#message-field').val('@'+f);
    $('#mention-hidden').val(mention);

    });


$('.file-input').change(function() {
    $file = $(this).val();
    $file = $file.replace(/.*[\/\\]/, ''); //grab only the file name not the path
    $('.filename-container').append("<span  class='filename' id='file-holder' style='display:'>" + $file + "</span>").show();
   
  });
});


//To select country name
 </script> 
 <script>
$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
     

    $('.next-step').click(function(e) {
      e.preventDefault();
        var review = $(this).val();
        var comment = $("#comment").val();
        var question = $(this).attr('id');
        var reviewQ = $("#reviewQ").val();
        var reviewUser = $("#reviewUser").val();
        var reviewChat = $("#reviewChat").val();
        var feedback1 = $("#feedback1").text();
        var feed1 =  feedback1.trim();
        var feedback2 = $("#feedback2").text();
        var feed2 =  feedback2.trim();
        var feedback3 = $("#feedback3").text();
        var feed3 =  feedback3.trim();
        var feedback4 = $("#feedback4").text();
        var feed4 =  feedback4.trim();
        var feedback5 = $("#feedback5").text();
        var feed5 =  feedback5.trim();
        var feedback6 = $("#feedback6").text();
        var feed6 =  feedback6.trim();
        $.ajax({
        type: "POST",
        url: "storereview",
        data:{review: review, question: question, Chat: reviewChat , QuestionsID: reviewQ, UserID:reviewUser,comment:comment},
        success: function(strData){
            var data = JSON.parse(strData);
            if(data.data === "OK"){ 
              if(question === "q1" && feed2.length > 0){
               var $active = $('.wizard .nav-tabs li.active');
               $active.next().removeClass('disabled');
               nextTab($active); 
           } else if(question === "q2" && feed3.length > 0){
               var $active = $('.wizard .nav-tabs li.active');
               $active.next().removeClass('disabled');
               nextTab($active); 
           }else if(question === "q3" && feed4.length > 0){
               var $active = $('.wizard .nav-tabs li.active');
               $active.next().removeClass('disabled');
               nextTab($active);
           }else if(question === "q4" && feed5.length > 0){
              var $active = $('.wizard .nav-tabs li.active');
               $active.next().removeClass('disabled');
               nextTab($active);
            }
           else{
               var $active = $('.wizard .nav-tabs li:nth-last-child(2)');
               $active.removeClass('disabled');
                lastTab($active); 
           }
           if(question ==="q6" && data.data === "OK"){
               var $active = $('.wizard .nav-tabs li:last');
               $active.removeClass('disabled');
                lastTab($active); 

            
           }
         }    
        }
        }); 
        // console.log("clicked");
         // var $active = $('.wizard .nav-tabs li.active');
         // $active.next().removeClass('disabled');
         // nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
  $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

function lastTab(elem) {
   $(elem).find('a[data-toggle="tab"]').click();
}

//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
    
    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});
    
    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');
    
    // The Accordion Effect
    $('.accordion-header').click(function () {
        if($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
        
        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });
    
    return false;
});

 </script>

    <script>
      $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: '../lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });

    </script>
    <script>


$(document).ready(function(){

  $("div.emoji-items").on('click',function(){
 setTimeout(function () {
       var $emoji = $('div.emoji-wysiwyg-editor').text();
       $('#message-field').val($('#message-field').val() + $emoji)
       $('div.emoji-wysiwyg-editor').text(" ");
        
        }, 10);
    
 });


document.addEventListener('DOMContentLoaded',function(){
  var trigger = document.getElementsByClassName("is-success")[0];
  var instance = new Tooltip(trigger,{
    title: trigger.getAttribute('data-tooltip'),
    trigger: "hover",
  });
});

  
});
    </script>
 
</body>
</html>
<?php $this->endPage() ?>
