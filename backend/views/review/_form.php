<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Edition;
use common\models\Chat;
use backend\models\ReviewAnswers;
use dosamigos\datepicker\DatePicker;
use dosamigos\ckeditor\CKEditor;
use kartik\time\TimePicker;
use frontend\models\User;


/* @var $this yii\web\View */
/* @var $model common\models\Chat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chat-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <?= $form->field($model, 'Chat')->dropDownList(ArrayHelper::map(Chat::find()->all(),'ID','Topic'),['prompt'=>'Select Chat']) ?>
    
        <?= $form->field($model, 'Question_1')->textInput() ?>
        <?= $form->field($model, 'Answer_set1')->dropDownList(ArrayHelper::map(ReviewAnswers::find()->all(),'id','Name'),['prompt'=>'Select Answer Set']) ?>

        <?= $form->field($model, 'Question_2')->textInput() ?>
        <?= $form->field($model, 'Answer_set2')->dropDownList(ArrayHelper::map(ReviewAnswers::find()->all(),'id','Name'),['prompt'=>'Select Answer Set']) ?>

         <?= $form->field($model, 'Question_3')->textInput() ?>
          <?= $form->field($model, 'Answer_set3')->dropDownList(ArrayHelper::map(ReviewAnswers::find()->all(),'id','Name'),['prompt'=>'Select Answer Set']) ?>

        <?= $form->field($model, 'Question_4')->textInput() ?>
           <?= $form->field($model, 'Answer_set4')->dropDownList(ArrayHelper::map(ReviewAnswers::find()->all(),'id','Name'),['prompt'=>'Select Answer Set']) ?>
    
         <?= $form->field($model, 'Question_5')->textInput() ?>
          <?= $form->field($model, 'Answer_set5')->dropDownList(ArrayHelper::map(ReviewAnswers::find()->all(),'id','Name'),['prompt'=>'Select Answer Set']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
