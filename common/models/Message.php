<?php

namespace common\models;

use Yii;
use backend\models\User;

/**
 * This is the model class for table "message".
 *
 * @property integer $ID
 * @property integer $Chat
 * @property integer $User
 * @property string $UserName
 * @property string $ChatTime
 * @property integer $Status
 * @property integer $MessageType
 * @property string $MessageText
 *
 * @property Attachment[] $attachments
 * @property Like[] $likes
 * @property Chat $chat
 * @property User $user
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Chat', 'User', 'UserName', 'MessageType', 'MessageText'], 'required'],
            [['Chat', 'User', 'Status', 'MessageType'], 'integer'],
            [['ChatTime'], 'safe'],
            [['MessageText'], 'string'],
            [['UserName'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Chat' => 'Chat',
            'User' => 'User',
            'UserName' => 'User Name',
            'ChatTime' => 'Chat Time',
            'Status' => 'Status',
            'MessageType' => 'Message Type',
            'MessageText' => 'Message Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['Message' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['MessageID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['ID' => 'Chat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'User']);
    }
}
