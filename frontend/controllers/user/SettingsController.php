<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace frontend\controllers\user;

use dektrium\user\Finder;
use dektrium\user\models\SettingsForm;
use dektrium\user\Module;
use dektrium\user\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use backend\models\Profile;

use dektrium\user\controllers\SettingsController as BaseSettingsController;

class SettingsController extends BaseSettingsController
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['profile', 'account', 'confirm', 'networks', 'disconnect','myprofile'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
          
    }

    public function actionMyprofile()
    {
        $model = $this->finder->findProfileById(Yii::$app->user->identity->getId());
         //var_dump($model);die;
         if($model == null){

            $user = Yii::$app->user->identity->username;
             $ldapObject = \Yii::$app->ad->search()->findBy('sAMAccountName',$user);
             
             $model = new Profile();
             $model->user_id = Yii::$app->user->identity->id;
             $model->name = $ldapObject['givenname'][0];
             $model->lastname = $ldapObject['sn'][0];
             $model->public_email = $ldapObject['mail'][0];
             $model->employee_id = $ldapObject['employeeid'][0];
             $model->manager = $ldapObject['manager'][0];
             $model->title = $ldapObject['title'][0];
             $model->branch = $ldapObject['physicaldeliveryofficename'][0];
             $model->role = 1;
             $model->organization= $ldapObject['department'][0];
             $model->designation= $ldapObject['department'][0];
             $model->country = $ldapObject['company'][0];
             
              $model->save();
              print_r($model->country);
              // var_dump($model->save(), $model->getErrors());die;
             return $this->render('myprofile', [
            'model' => $model,
        ]);
         }else{
             $user = Yii::$app->user->identity->username;
             $ldapObject = \Yii::$app->ad->search()->findBy('sAMAccountName',$user);
             
             $model = Profile::findOne(Yii::$app->user->identity->id);
            $model->user_id = Yii::$app->user->identity->id;
             $model->name = $ldapObject['givenname'][0];
             $model->lastname = $ldapObject['sn'][0];
             $model->public_email = $ldapObject['mail'][0];
             $model->employee_id = $ldapObject['employeeid'][0];
             $model->manager = $ldapObject['manager'][0];
             $model->title = $ldapObject['title'][0];
             $model->branch = $ldapObject['physicaldeliveryofficename'][0];
             $model->role = 1;
             $model->organization= $ldapObject['department'][0];
             $model->designation= $ldapObject['department'][0];
             $model->country = $ldapObject['company'][0];
             
              $model->save();
           return $this->render('myprofile', [
            'model' => $model,
          ]);

         }
    }
}