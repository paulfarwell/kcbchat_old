<?php 

namespace backend\models;

use dektrium\user\models\User as BaseUser;

class User extends BaseUser
{
    public function init() {
        $this->on(self::BEFORE_REGISTER, function() {
            $this->username = $this->email;
        });

        parent::init();
    }

    public function rules() {
        $rules = parent::rules();
        unset($rules['usernameRequired']);
        return $rules;
    }
    
    public function getCeoString()
    {
    	if ($this->profile!==null) {
    		if (isset($this->profile->name)) {
    			return $this->profile->name . " " . $this->profile->lastname ;
    		}
    	}
    }
}