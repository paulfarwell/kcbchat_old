<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<div class="row">
	<section class="parent chat-details-container">
	   
	   <div class="container-fluid" style="padding-left:5px;"> 
	    
	    <div class="main-chat-page row">
	        
	       


	        <?= $content ?>
	   


	        <!-- END conversation-body -->
	        </div> <!-- conversation-body  -->
	                
	    </div><!-- main row -->
	  </div><!-- main container -->
	</section> <!-- container-section -->
</div>
<?php $this->endContent();