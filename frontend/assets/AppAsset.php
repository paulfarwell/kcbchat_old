<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/perfect-scrollbar.min.css',
        'css/flat-bootstrap.css',
        // 'css/animate.css',
        'css/default.css',
        'css/main.css',
        'css/mobile.css',
        'css/jquery.css',
        '/lib/css/emoji.css',
        // '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        // '//fonts.googleapis.com/css?family=Montserrat:400,700',
        // '//fonts.googleapis.com/css?family=Kaushan+Script',
        // '//fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic',
        // '//fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700',

    ];
    public $js = [
        'js/moment.js',
        'js/perfectscrollbar.min.js',
        'js/jquery-ui.min.js',
        'js/autocomplete.js',
        '/lib/js/config.js',
        '/lib/js/util.js',
        '/lib/js/jquery.emojiarea.js',
        '/lib/js/emoji-picker.js',
        
         
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        'Zelenin\yii\SemanticUI\assets\SemanticUICSSAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}