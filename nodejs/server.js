 var app = require('express')();
var mysql = require('mysql');
var server = require('http').Server(app);
var io = require('socket.io')(server);

// var redis = require('redis');
var db = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '6Qx1sWS0![oO', 
  //KEN
  //password : '6Qx1sWS0',
  // EVANS
  //password : 'r6r5bb!!',
  database :'kcbchat',
  charset : 'utf8mb4',
});

db.connect(function(err){
    if (err) console.log(err)
})

server.listen(443);
/*
var socketCount = 0

io.on('connection', function (socket) {

    // Socket has connected, increase socket count
    socketCount++
    // Let all sockets know how many are connected
    io.sockets.emit('users connected', socketCount)


    socket.on('disconnect', function() {
        // Decrease the socket count on a disconnect, emit
        socketCount--
        io.sockets.emit('users connected', socketCount)
    })
});

*/

var chat = io.of('/socket').on('connection', function (socket) {
   
    socket.on('room', function(room) {
        var activestatus = db.query('SELECT Status FROM online WHERE User = ? AND CurrentChat = ?', [room.userID,room.room ], function (err, result) {
            if(err===null){
                isactive=result[0];
                //console.log(isactive.Status);
                if(typeof(isactive.Status)!=='undefined'){
                    if (isactive.Status ===1) {
                        socket.activeinchat = true;
                    }else{
                        socket.activeinchat = false;
                    }
                }
                
            }
            else{
                 console.log(err);
            }
            
        });
        socket.join(room.room);
        socket.room = room.room;
    });

    socket.on('login', function(data) {
        db.query('SELECT p.name,p.lastname ,p.avatar,p.role, u.id FROM profile p INNER JOIN user u ON p.user_id = u.id WHERE u.auth_key = ?', [data.key], function(err, result) {
                console.log(result)
                //var joinedUsers = chat.cli
                var joinedUsers =  findClientsSocketByRoomId(data.id).length;
                var maxUsers = data.maxUsers;
                //console.log("Max users : " + maxUsers);
                if (joinedUsers > maxUsers ) {
                    socket.canpost = false;
                }else{
                    socket.canpost = true;
                }
                
                if(err===null){
                    
                    userData=userResult=result[0];
                    if(typeof(userResult.name)!=='undefined'){
                        
                       
                            //found and can assign                  
                            socket.userName=userResult.name + ' ' + userResult.lastname;
                            //console.log(socket.userName + ": Can Post " + socket.canpost);
                            if (userResult.avatar ===null) {
                            	noimage = "user.jpg";
                                socket.avatar="/images/avatar/" +noimage;
                            }else if(userResult.avatar!='' && typeof(userResult.avatar)!=='undefined'){
                                socket.avatar="/images/avatar/"+userResult.avatar;
                            }
                            else{
                            	noimage = "user.jpg";
                                socket.avatar="/images/avatar/" +noimage;
                            }
                            socket.chatStatus=0;

                            //If user has a participate request for this chat 
                            /*
                            if(userResult.Chat_Id==data.id){
                                
                                socket.chatStatus=userResult.status;
                            }
                            else{
                                socket.chatStatus=0;
                            }
                            */
                            
                            
                            socket.role=userResult.role;
                            socket.userid=userResult.id;
                            onlineusers= io.nsps['/socket'].adapter.rooms[data.id].length;
                            if (typeof(onlineusers)!=='undefined') {
                                socket.emit('onlineusers',onlineusers);
                                socket.broadcast.to(data.id).emit("onlineusers",onlineusers);
                                  console.log("active");
                            };
                            socket.cssClass='me';
                            socket.emit('roommates',getPeopleinChat(data.id, '/socket') );
                            socket.emit("newuser",{Name:socket.userName,avatar:socket.avatar,userid:userResult.id, activeStatus: socket.activeinchat});
                            socket.broadcast.to(data.id).emit("newuser",{Name:socket.userName,avatar:socket.avatar,userid:userResult.id, activeStatus: socket.activeinchat});
                            //console.log("userjoined" + userResult.role);
                    }
                    else{
                            socket.emit('verificationError');
                            console.log(err)    
                    }
                }
                else{
                    socket.emit('verificationError');
                    console.log(err)
                }
                //console.log(socket.avatar);
            }); 
    });

    socket.on('newmessage', function(data){
        // console.log(data);
       if(data.likes > 0){
          var insertdata={UserID:data.userID,MessageID:data.likes,Name:data.user};
          var userhasliked = db.query('SELECT COUNT(*) as hasliked FROM `like` WHERE UserID = ? AND MessageID = ?', [data.userID,data.likes ], function (err, results){
             if(err===null){
                if (results[0].hasliked < 1) {
                    var query= db.query('INSERT INTO `like` SET ?', insertdata, function (err, result) {                
                         if (err===null) {
                            //var myMsgid=data.msgid;
                            var sql = 'SELECT * FROM `like` WHERE MessageID = ?';
                             db.query(sql, [data.likes], function(err, resultss) {
                              if (err===null) {
                                   console.log(resultss)
                                   numrows = resultss.length;
                                   socket.emit('likemsgid',{likedmsgid:data.likes,likedmsgcount:numrows,likingUser:resultss[0].Name});
                                }else{
                                    console.log(err);
                                }
                            });
                        }else{
                            console.log(err);
                        }
                     });        
                }else{
                    socket.emit('likemsgid',{likedmsgid:data.likes, liked:results[0].hasliked});
                }
                // console.log(results[0].hasliked);          
            }
             else{
                 console.log(err);
                 socket.emit('verificationError');
            }

        }); 
       }
    else{
          var date = new Date().toISOString().slice(0, 19).replace('T', ' ');
        var postedData= {Chat: socket.room, UserName:data.user, User:data.userID, MessageText:data.msg, Status:1, MessageType:1,replyID:data.replyID, role:data.userRole, mentionID:data.mentionID};
        if(socket.canpost!=false){          
            var query = db.query('INSERT INTO message SET ?', postedData, function (err, result) {
                if(err===null){
                  var query = db.query('SELECT avatar FROM profile WHERE user_id  = ? ', [data.userID], function(err,resultss) {
                    if(err ===null){
                    
                     if(data.replyID > 0){
                        var query = db.query('SELECT * FROM message ', function (err,results,field ) {
                            //console.log(data.replyID)
                             results.forEach(function(testRepMsg){
                               if(testRepMsg.ID === data.replyID){

                                socket.broadcast.to(socket.room).emit('receive', {msg: data.msg,  user: data.user,msgid:result.insertId, avatar:resultss[0].avatar ,userRole:data.userRole,reply: testRepMsg.replyID, replyUser: testRepMsg.UserName,replyMsg: testRepMsg.MessageText});
                                socket.emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: resultss[0].avatar,userRole:data.userRole,reply: testRepMsg.replyID, replyUser: testRepMsg.UserName,replyMsg: testRepMsg.MessageText});
                               }
                           });   
                        });
                     }else if(data.mentionID > 0){
                      var query = db.query('SELECT * FROM profile ', function (err,resul,field ) {
                           
                             resul.forEach(function(mentionMsg){
                               if(mentionMsg.user_id === data.mentionID){

                                socket.broadcast.to(socket.room).emit('receive', {msg: data.msg,  user: data.user,msgid:result.insertId, avatar:resultss[0].avatar ,userRole:data.userRole,reply: data.replyID, mentionID:data.mentionID, mentionName: mentionMsg.name,mentionLastname: mentionMsg.lastname});
                                socket.emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: resultss[0].avatar,userRole:data.userRole,reply: data.replyID, mentionID:data.mentionID, mentionName: mentionMsg.name,mentionLastname: mentionMsg.lastname});
                               }
                           });   
                        });


                     }else if(data.upload != null){
                        var fileUpload = {Message:result.insertId, Attachment:data.upload, AttachmentName:data.upload, AttachmentType:data.uploadType}
                       var query = db.query('INSERT INTO attachment SET ?', fileUpload, function (err,resu) {
                         if (err === null){
                           var query = db.query('SELECT * FROM attachment ', function (err,res,field ) {
                             res.forEach(function(attachment){
                               if(attachment.ID === resu.insertId){
                                  socket.broadcast.to(socket.room).emit('receive', {msg: data.msg,  user: data.user,msgid:result.insertId, avatar:resultss[0].avatar ,userRole:data.userRole,reply: data.replyID, mentionID:data.mentionID, attachmentID: attachment.ID });
                                 socket.emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: resultss[0].avatar,userRole:data.userRole,reply: data.replyID, mentionID:data.mentionID, attachmentID:attachment.ID});
                               }
                           });
                         });
                           
                         }
                       });
                      }
                     else{
                      socket.broadcast.to(socket.room).emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: resultss[0].avatar ,userRole:data.userRole,replyID:data.replyID,mentionID:data.mentionID});
                      socket.emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: resultss[0].avatar,userRole:data.userRole,replyID:data.replyID,mentionID:data.mentionID});
                    }
                    // console.log("New message from: " + data.user + ". In  Room " + socket.room + " Message :-> " + data.msg);
                     //socket.broadcast.to(socket.room).emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, avatar: data.avatar ,userRole:data.userRole});
                      //socket.emit('receive', {msg: data.msg, user: data.user,msgid:result.insertId, img: data.img,userRole:data.userRole,replyID:data.replyID});
                     if (socket.activeinchat==false) {
                        socket.activeinchat = true;
                        //socket.emit('roommates',getPeopleinChat(socket.room, '/socket') );
                        socket.emit("newuser",{Name:socket.userName,avatar:socket.avatar,userid:socket.userid, activeStatus: socket.activeinchat});
                        socket.broadcast.to(socket.room).emit("newuser",{Name:socket.userName,avatar:socket.avatar,userid:socket.userid, activeStatus: socket.activeinchat});
                        var activateuser= db.query('UPDATE online SET Status = ? WHERE User = ? AND CurrentChat = ?', [1,data.userID,socket.room ], function (err, result) {
                            if(err===null){
                                socket.activeinchat==true;
                             }
                             else{
                                 console.log(err);
                             }
                            
                        });
                     }
                  }
                  });
                 }
                 else{
                     console.log(err);
                 }
                
            });
        


        }
    }
        
        // console.log(socket.activeinchat);
        // When the server receives a message, it sends it to the other person in the room.
        // socket.broadcast.to(socket.room).emit('receive', {msg: data.msg, user: data.user, img: data.img});
        
    });

    var subscriberchannel = 'notification_'+socket.room;

    // var redisClient = redis.createClient();

    // redisClient.subscribe('notification');
/*
    redisClient.on("message", function(channel, message) {      
        console.log("New message: " + message + ". In channel: " + channel + ". Room " + socket.room);
        socket.broadcast.to(socket.room).emit(channel, message);
        
    });
*/
    socket.on("message", function(channel, message) {
        var messagesent = JSON.parse(message);
        // console.log(messagesent);
        var chatmessage = {Chat:messagesent.chatRoom, User:messagesent.chatUserId, UserName: messagesent.chatUserName, Status:1, MessageType:1, MessageText:messagesent.message};
        var query = db.query('INSERT INTO message SET ?', chatmessage, function (err, result) {
                if(err===null){
                    console.log(chatmessage);                
                }
                 else{
                     console.log(err);
                }
        });
    });
    socket.on('receiveOldMsg', function(data){
        var ChatID=data.chatRoom.id;
        db.query('SELECT m.ID, m.UserName, m.ChatTime, m.MessageText, m.replyID, m.mentionID, m.role, p.avatar, (SELECT ID FROM `attachment` l WHERE l.Message=m.ID) as attachment, (SELECT COUNT(*) FROM `like` l WHERE l.MessageID=m.ID) as likes FROM  `message` m INNER JOIN user u ON u.id = m.User INNER JOIN profile p ON p.user_id = m.User  WHERE m.Chat = ? ORDER BY m.ID ASC', [ChatID], function(err, results) {
            if(err===null){
                 // console.log(results);
                    db.query('SELECT * FROM `like` ', function(err,resultss){
                        socket.emit('oldMsgs',{theMsgs:results,like:resultss})
                    });              
            }
             else{
                 console.log(err);
            }

        });
    });

    // socket.on('showlikes', function(data){
    //    console.log(data);
    // });

    socket.on('likes', function(data){
        // console.log(data);
         var likecount=0;
        var insertdata={UserID:data.chatRoom.chatUserId,MessageID:data.msgid}
        // console.log(insertdata)
        //check if user has liked before
        var userhasliked = db.query('SELECT COUNT(*) as hasliked FROM `like` WHERE UserID = ? AND MessageID = ?', [data.chatRoom.chatUserId,data.msgid ], function (err, results){
             if(err===null){
                if (results[0].hasliked < 1) {
                    var query= db.query('INSERT INTO `like` SET ?', insertdata, function (err, result) {                
                         if (err===null) {
                            //var myMsgid=data.msgid;
                            var sql = 'SELECT COUNT(*) as totalcount FROM `like` WHERE MessageID = ?';
                            db.query(sql, [data.msgid], function(err, result) {
                              if (err===null) {
                                // console.log(result);
                                   socket.broadcast.to(socket.room).emit('receive', {likedmsgid:data.msgid,likedmsgcount:result[0].totalcount});
                                  //socket.emit('receive', {likedmsgid:data.msgid,likedmsgcount:result[0].totalcount});
                                }else{
                                    console.log(err);
                                }
                            });
                        }else{
                            console.log(err);
                        }
                     });        
                }else{
                    socket.emit('likemsgid',{likedmsgid:data.msgid, liked:results[0].hasliked});
                }
                // console.log(results[0].hasliked);          
            }
             else{
                 console.log(err);
                 socket.emit('verificationError');
            }

        })
        
         
        socket.broadcast.to(socket.room).emit('likemsgid', {likedmsgid:data.msgid,likedmsgcount:likecount});

    });


     
    // socket.on('disconnect', function() {
    //     console.log("disconnect: ", socket.id);
    //     /*
    //     * to update
    //     * set user as inactive when he or she leaves the chat
    //     * Update to remove user from active and online list
    //     */
        
    //     var activateuser= db.query('UPDATE online SET Status = ? WHERE User = ? AND CurrentChat = ?', [0,socket.userid,socket.room ], function (err, result) {
    //         if(err===null){
    //             socket.activeinchat==false;
    //          }
    //          else{
    //              console.log(err);
    //          }
    //     });
        
    //     //socket.leave(socket.room);
    //     // redisClient.quit();
    // });

    function getPeopleinChat(roomId, namespace){
        var usersArray=[]
       , ns = io.of(namespace ||"/");    // the default namespace is "/"

        if (ns) {
            for (var id in ns.connected) {
                if(roomId) {
                    //var index = ns.connected[id].rooms.indexOf(roomId) ;
                    if((typeof(ns.connected[id].userid) !== 'undefined') && (ns.connected[id].room ==roomId)) {
                        //res.push(ns.connected[id].avatar);
                        usersArray.push({avatar:ns.connected[id].avatar, Name:ns.connected[id].userName, userid:ns.connected[id].userid, activeStatus:ns.connected[id].activeinchat, room:roomId});
                    }
                } else {
                    usersArray.push({avatar:ns.connected[id].avatar, Name:ns.connected[id].userName, userid:ns.connected[id].userid, activeStatus:ns.connected[id].activeinchat, room:roomId});
                }
            }
        }
        return usersArray;
    }
    function findClientsSocketByRoomId(roomId) {
        var res = []
        , room = io.sockets.adapter.rooms[roomId];
        if (room) {
            for (var id in room) {
            res.push(io.sockets.adapter.nsp.connected[id]);
            }
        }
        return res;
    }

    function findClientsSocket(roomId, namespace) {
        var res = []
        , ns = io.of(namespace ||"/");    // the default namespace is "/"

        if (ns) {
            for (var id in ns.connected) {
                if(roomId) {
                    //var index = ns.connected[id].rooms.indexOf(roomId) ;
                    if(typeof(ns.connected[id].userid) !== 'undefined') {
                        //res.push(ns.connected[id].avatar);
                        res.push({avatar:ns.connected[id].avatar, Name:ns.connected[id].userName, userid:ns.connected[id].userid, activeStatus:ns.connected[id].activeinchat, room:roomId});
                    }
                } else {
                    res.push(ns.connected[id]);
                }
            }
        }
        return res;
    }

});
